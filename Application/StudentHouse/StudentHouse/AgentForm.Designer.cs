﻿namespace StudentHouse
{
    partial class AgentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AgentForm));
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblRoomNo = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPhoneNo = new System.Windows.Forms.Label();
            this.tbEmailAddress = new System.Windows.Forms.TextBox();
            this.tbTelNo = new System.Windows.Forms.TextBox();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.lbxResidentsInfo = new System.Windows.Forms.ListBox();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.chbAnnounce = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbRules = new System.Windows.Forms.TextBox();
            this.btnClearSelected = new System.Windows.Forms.Button();
            this.pnlMessages = new System.Windows.Forms.Panel();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.cbMarkAsEvent = new System.Windows.Forms.CheckBox();
            this.btnSendAnnouncement = new System.Windows.Forms.Button();
            this.tbAnnouncementbody = new System.Windows.Forms.TextBox();
            this.metrotbMain = new MetroFramework.Controls.MetroTabControl();
            this.metrotpResidents = new MetroFramework.Controls.MetroTabPage();
            this.btnRep = new System.Windows.Forms.Button();
            this.dtpLeaveDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.cbxRoomNo = new System.Windows.Forms.ComboBox();
            this.btnShowUserDetails = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lblLeaveDate = new System.Windows.Forms.Label();
            this.lblEndContractDate = new System.Windows.Forms.Label();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.metrotpRules = new MetroFramework.Controls.MetroTabPage();
            this.metrotpMessages = new MetroFramework.Controls.MetroTabPage();
            this.metrotpAnnouncements = new MetroFramework.Controls.MetroTabPage();
            this.dtpEndTimeAnn = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDateAnn = new System.Windows.Forms.DateTimePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.dtpStartTimeAnn = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDateAnn = new System.Windows.Forms.DateTimePicker();
            this.btnExit = new System.Windows.Forms.Button();
            this.metrotbMain.SuspendLayout();
            this.metrotpResidents.SuspendLayout();
            this.metrotpRules.SuspendLayout();
            this.metrotpMessages.SuspendLayout();
            this.metrotpAnnouncements.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAdd.Location = new System.Drawing.Point(30, 374);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(87, 29);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnUpdate.Enabled = false;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnUpdate.Location = new System.Drawing.Point(121, 374);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(87, 29);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblRoomNo
            // 
            this.lblRoomNo.AutoSize = true;
            this.lblRoomNo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblRoomNo.Location = new System.Drawing.Point(292, 237);
            this.lblRoomNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRoomNo.Name = "lblRoomNo";
            this.lblRoomNo.Size = new System.Drawing.Size(72, 19);
            this.lblRoomNo.TabIndex = 11;
            this.lblRoomNo.Text = "Room No";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lblEmail.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEmail.Location = new System.Drawing.Point(28, 128);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(102, 19);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email Address";
            // 
            // lblPhoneNo
            // 
            this.lblPhoneNo.AutoSize = true;
            this.lblPhoneNo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPhoneNo.Location = new System.Drawing.Point(231, 127);
            this.lblPhoneNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhoneNo.Name = "lblPhoneNo";
            this.lblPhoneNo.Size = new System.Drawing.Size(104, 19);
            this.lblPhoneNo.TabIndex = 9;
            this.lblPhoneNo.Text = "Telephone No";
            // 
            // tbEmailAddress
            // 
            this.tbEmailAddress.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbEmailAddress.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbEmailAddress.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbEmailAddress.Location = new System.Drawing.Point(30, 148);
            this.tbEmailAddress.Margin = new System.Windows.Forms.Padding(2);
            this.tbEmailAddress.Name = "tbEmailAddress";
            this.tbEmailAddress.Size = new System.Drawing.Size(150, 20);
            this.tbEmailAddress.TabIndex = 3;
            // 
            // tbTelNo
            // 
            this.tbTelNo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbTelNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbTelNo.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbTelNo.Location = new System.Drawing.Point(236, 148);
            this.tbTelNo.Margin = new System.Windows.Forms.Padding(2);
            this.tbTelNo.Name = "tbTelNo";
            this.tbTelNo.Size = new System.Drawing.Size(150, 20);
            this.tbTelNo.TabIndex = 4;
            // 
            // tbFirstName
            // 
            this.tbFirstName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbFirstName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbFirstName.Location = new System.Drawing.Point(30, 90);
            this.tbFirstName.Margin = new System.Windows.Forms.Padding(2);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(150, 20);
            this.tbFirstName.TabIndex = 1;
            // 
            // lbxResidentsInfo
            // 
            this.lbxResidentsInfo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lbxResidentsInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbxResidentsInfo.FormattingEnabled = true;
            this.lbxResidentsInfo.ItemHeight = 17;
            this.lbxResidentsInfo.Location = new System.Drawing.Point(560, 46);
            this.lbxResidentsInfo.Margin = new System.Windows.Forms.Padding(2);
            this.lbxResidentsInfo.Name = "lbxResidentsInfo";
            this.lbxResidentsInfo.Size = new System.Drawing.Size(287, 340);
            this.lbxResidentsInfo.TabIndex = 3;
            this.lbxResidentsInfo.SelectedIndexChanged += new System.EventHandler(this.lbxResidentsInfo_SelectedIndexChanged);
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lblFirstName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFirstName.Location = new System.Drawing.Point(28, 72);
            this.lblFirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(79, 19);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "First Name";
            // 
            // chbAnnounce
            // 
            this.chbAnnounce.AutoSize = true;
            this.chbAnnounce.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.chbAnnounce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chbAnnounce.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.chbAnnounce.Location = new System.Drawing.Point(622, 457);
            this.chbAnnounce.Margin = new System.Windows.Forms.Padding(2);
            this.chbAnnounce.Name = "chbAnnounce";
            this.chbAnnounce.Size = new System.Drawing.Size(183, 23);
            this.chbAnnounce.TabIndex = 2;
            this.chbAnnounce.Text = "Announce the change";
            this.chbAnnounce.UseVisualStyleBackColor = false;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(809, 453);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 33);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbRules
            // 
            this.tbRules.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbRules.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbRules.Location = new System.Drawing.Point(7, 12);
            this.tbRules.Margin = new System.Windows.Forms.Padding(2);
            this.tbRules.Multiline = true;
            this.tbRules.Name = "tbRules";
            this.tbRules.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRules.Size = new System.Drawing.Size(875, 426);
            this.tbRules.TabIndex = 0;
            // 
            // btnClearSelected
            // 
            this.btnClearSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnClearSelected.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnClearSelected.FlatAppearance.BorderSize = 0;
            this.btnClearSelected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearSelected.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnClearSelected.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClearSelected.Location = new System.Drawing.Point(733, 37);
            this.btnClearSelected.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearSelected.Name = "btnClearSelected";
            this.btnClearSelected.Size = new System.Drawing.Size(128, 37);
            this.btnClearSelected.TabIndex = 15;
            this.btnClearSelected.Text = "Clear Selected";
            this.btnClearSelected.UseVisualStyleBackColor = false;
            this.btnClearSelected.Click += new System.EventHandler(this.btnClearSelected_Click);
            // 
            // pnlMessages
            // 
            this.pnlMessages.AutoScroll = true;
            this.pnlMessages.BackColor = System.Drawing.Color.Transparent;
            this.pnlMessages.Location = new System.Drawing.Point(28, 37);
            this.pnlMessages.Margin = new System.Windows.Forms.Padding(2);
            this.pnlMessages.Name = "pnlMessages";
            this.pnlMessages.Size = new System.Drawing.Size(675, 418);
            this.pnlMessages.TabIndex = 2;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.lblTime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblTime.Location = new System.Drawing.Point(208, 407);
            this.lblTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(74, 19);
            this.lblTime.TabIndex = 6;
            this.lblTime.Text = "Start time";
            this.lblTime.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.lblDate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblDate.Location = new System.Drawing.Point(5, 407);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(78, 19);
            this.lblDate.TabIndex = 5;
            this.lblDate.Text = "Start date";
            this.lblDate.Visible = false;
            // 
            // cbMarkAsEvent
            // 
            this.cbMarkAsEvent.AutoSize = true;
            this.cbMarkAsEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbMarkAsEvent.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.cbMarkAsEvent.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.cbMarkAsEvent.Location = new System.Drawing.Point(7, 373);
            this.cbMarkAsEvent.Margin = new System.Windows.Forms.Padding(2);
            this.cbMarkAsEvent.Name = "cbMarkAsEvent";
            this.cbMarkAsEvent.Size = new System.Drawing.Size(121, 23);
            this.cbMarkAsEvent.TabIndex = 2;
            this.cbMarkAsEvent.Text = "Mark as Event";
            this.cbMarkAsEvent.UseVisualStyleBackColor = true;
            this.cbMarkAsEvent.CheckedChanged += new System.EventHandler(this.cbMarkAsEvent_CheckedChanged);
            // 
            // btnSendAnnouncement
            // 
            this.btnSendAnnouncement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnSendAnnouncement.FlatAppearance.BorderSize = 0;
            this.btnSendAnnouncement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendAnnouncement.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnSendAnnouncement.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSendAnnouncement.Location = new System.Drawing.Point(787, 418);
            this.btnSendAnnouncement.Margin = new System.Windows.Forms.Padding(2);
            this.btnSendAnnouncement.Name = "btnSendAnnouncement";
            this.btnSendAnnouncement.Size = new System.Drawing.Size(95, 41);
            this.btnSendAnnouncement.TabIndex = 1;
            this.btnSendAnnouncement.Text = "Send";
            this.btnSendAnnouncement.UseVisualStyleBackColor = false;
            this.btnSendAnnouncement.Click += new System.EventHandler(this.btnSendAnnouncement_Click);
            // 
            // tbAnnouncementbody
            // 
            this.tbAnnouncementbody.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbAnnouncementbody.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbAnnouncementbody.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnnouncementbody.Location = new System.Drawing.Point(7, 15);
            this.tbAnnouncementbody.Margin = new System.Windows.Forms.Padding(2);
            this.tbAnnouncementbody.Multiline = true;
            this.tbAnnouncementbody.Name = "tbAnnouncementbody";
            this.tbAnnouncementbody.Size = new System.Drawing.Size(872, 336);
            this.tbAnnouncementbody.TabIndex = 0;
            // 
            // metrotbMain
            // 
            this.metrotbMain.Controls.Add(this.metrotpResidents);
            this.metrotbMain.Controls.Add(this.metrotpRules);
            this.metrotbMain.Controls.Add(this.metrotpMessages);
            this.metrotbMain.Controls.Add(this.metrotpAnnouncements);
            this.metrotbMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.metrotbMain.ItemSize = new System.Drawing.Size(150, 34);
            this.metrotbMain.Location = new System.Drawing.Point(0, 0);
            this.metrotbMain.Name = "metrotbMain";
            this.metrotbMain.SelectedIndex = 3;
            this.metrotbMain.Size = new System.Drawing.Size(894, 545);
            this.metrotbMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.metrotbMain.Style = MetroFramework.MetroColorStyle.FourstandingGreen;
            this.metrotbMain.TabIndex = 1;
            this.metrotbMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metrotbMain.Theme = MetroFramework.MetroThemeStyle.Fourstanding;
            this.metrotbMain.UseSelectable = true;
            this.metrotbMain.SelectedIndexChanged += new System.EventHandler(this.metrotbMain_SelectedIndexChanged);
            // 
            // metrotpResidents
            // 
            this.metrotpResidents.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpResidents.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.metrotpResidents.Controls.Add(this.btnRep);
            this.metrotpResidents.Controls.Add(this.dtpLeaveDate);
            this.metrotpResidents.Controls.Add(this.dtpEndDate);
            this.metrotpResidents.Controls.Add(this.dtpStartDate);
            this.metrotpResidents.Controls.Add(this.cbxRoomNo);
            this.metrotpResidents.Controls.Add(this.btnAdd);
            this.metrotpResidents.Controls.Add(this.btnShowUserDetails);
            this.metrotpResidents.Controls.Add(this.btnClear);
            this.metrotpResidents.Controls.Add(this.btnRemove);
            this.metrotpResidents.Controls.Add(this.btnUpdate);
            this.metrotpResidents.Controls.Add(this.lblRoomNo);
            this.metrotpResidents.Controls.Add(this.lbxResidentsInfo);
            this.metrotpResidents.Controls.Add(this.lblLeaveDate);
            this.metrotpResidents.Controls.Add(this.lblEndContractDate);
            this.metrotpResidents.Controls.Add(this.lblStartDate);
            this.metrotpResidents.Controls.Add(this.lblEmail);
            this.metrotpResidents.Controls.Add(this.tbLastName);
            this.metrotpResidents.Controls.Add(this.tbFirstName);
            this.metrotpResidents.Controls.Add(this.lblPhoneNo);
            this.metrotpResidents.Controls.Add(this.lblLastName);
            this.metrotpResidents.Controls.Add(this.lblFirstName);
            this.metrotpResidents.Controls.Add(this.tbEmailAddress);
            this.metrotpResidents.Controls.Add(this.tbTelNo);
            this.metrotpResidents.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.metrotpResidents.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.metrotpResidents.HorizontalScrollbarBarColor = true;
            this.metrotpResidents.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpResidents.HorizontalScrollbarSize = 10;
            this.metrotpResidents.Location = new System.Drawing.Point(4, 38);
            this.metrotpResidents.Name = "metrotpResidents";
            this.metrotpResidents.Size = new System.Drawing.Size(886, 503);
            this.metrotpResidents.TabIndex = 0;
            this.metrotpResidents.Text = "Residents";
            this.metrotpResidents.Theme = MetroFramework.MetroThemeStyle.Fourstanding;
            this.metrotpResidents.UseCustomBackColor = true;
            this.metrotpResidents.UseCustomForeColor = true;
            this.metrotpResidents.UseStyleColors = true;
            this.metrotpResidents.VerticalScrollbarBarColor = true;
            this.metrotpResidents.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpResidents.VerticalScrollbarSize = 10;
            // 
            // btnRep
            // 
            this.btnRep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnRep.FlatAppearance.BorderSize = 0;
            this.btnRep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRep.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnRep.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRep.Location = new System.Drawing.Point(709, 420);
            this.btnRep.Margin = new System.Windows.Forms.Padding(2);
            this.btnRep.Name = "btnRep";
            this.btnRep.Size = new System.Drawing.Size(138, 44);
            this.btnRep.TabIndex = 14;
            this.btnRep.Text = "Set As Rep";
            this.btnRep.UseVisualStyleBackColor = false;
            this.btnRep.Click += new System.EventHandler(this.btnRep_Click);
            // 
            // dtpLeaveDate
            // 
            this.dtpLeaveDate.CustomFormat = "yyyy-MM-dd";
            this.dtpLeaveDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpLeaveDate.Location = new System.Drawing.Point(30, 258);
            this.dtpLeaveDate.Name = "dtpLeaveDate";
            this.dtpLeaveDate.Size = new System.Drawing.Size(150, 24);
            this.dtpLeaveDate.TabIndex = 7;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "yyyy-MM-dd";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(236, 202);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(150, 24);
            this.dtpEndDate.TabIndex = 6;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "yyyy-MM-dd";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(30, 202);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(150, 24);
            this.dtpStartDate.TabIndex = 5;
            // 
            // cbxRoomNo
            // 
            this.cbxRoomNo.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbxRoomNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxRoomNo.FormattingEnabled = true;
            this.cbxRoomNo.ItemHeight = 17;
            this.cbxRoomNo.Items.AddRange(new object[] {
            "11",
            "12",
            "13",
            "21",
            "22",
            "23",
            "31",
            "32"});
            this.cbxRoomNo.Location = new System.Drawing.Point(293, 258);
            this.cbxRoomNo.Margin = new System.Windows.Forms.Padding(2);
            this.cbxRoomNo.Name = "cbxRoomNo";
            this.cbxRoomNo.Size = new System.Drawing.Size(92, 25);
            this.cbxRoomNo.TabIndex = 8;
            // 
            // btnShowUserDetails
            // 
            this.btnShowUserDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnShowUserDetails.Enabled = false;
            this.btnShowUserDetails.FlatAppearance.BorderSize = 0;
            this.btnShowUserDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShowUserDetails.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnShowUserDetails.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnShowUserDetails.Location = new System.Drawing.Point(560, 420);
            this.btnShowUserDetails.Margin = new System.Windows.Forms.Padding(2);
            this.btnShowUserDetails.Name = "btnShowUserDetails";
            this.btnShowUserDetails.Size = new System.Drawing.Size(138, 44);
            this.btnShowUserDetails.TabIndex = 11;
            this.btnShowUserDetails.Text = "Show User Details";
            this.btnShowUserDetails.UseVisualStyleBackColor = false;
            this.btnShowUserDetails.Click += new System.EventHandler(this.btnShowUserDetails_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnClear.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnClear.Location = new System.Drawing.Point(303, 374);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(87, 29);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnRemove.Enabled = false;
            this.btnRemove.FlatAppearance.BorderSize = 0;
            this.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemove.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnRemove.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRemove.Location = new System.Drawing.Point(212, 374);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(87, 29);
            this.btnRemove.TabIndex = 10;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lblLeaveDate
            // 
            this.lblLeaveDate.AutoSize = true;
            this.lblLeaveDate.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lblLeaveDate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblLeaveDate.Location = new System.Drawing.Point(28, 238);
            this.lblLeaveDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLeaveDate.Name = "lblLeaveDate";
            this.lblLeaveDate.Size = new System.Drawing.Size(89, 19);
            this.lblLeaveDate.TabIndex = 10;
            this.lblLeaveDate.Text = "Leave Date";
            // 
            // lblEndContractDate
            // 
            this.lblEndContractDate.AutoSize = true;
            this.lblEndContractDate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEndContractDate.Location = new System.Drawing.Point(231, 180);
            this.lblEndContractDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEndContractDate.Name = "lblEndContractDate";
            this.lblEndContractDate.Size = new System.Drawing.Size(74, 19);
            this.lblEndContractDate.TabIndex = 10;
            this.lblEndContractDate.Text = "End Date";
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.lblStartDate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblStartDate.Location = new System.Drawing.Point(28, 180);
            this.lblStartDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(78, 19);
            this.lblStartDate.TabIndex = 10;
            this.lblStartDate.Text = "Start Date";
            // 
            // tbLastName
            // 
            this.tbLastName.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbLastName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbLastName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.tbLastName.Location = new System.Drawing.Point(236, 92);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(2);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(150, 20);
            this.tbLastName.TabIndex = 2;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblLastName.Location = new System.Drawing.Point(232, 72);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(81, 19);
            this.lblLastName.TabIndex = 0;
            this.lblLastName.Text = "Last Name";
            // 
            // metrotpRules
            // 
            this.metrotpRules.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpRules.Controls.Add(this.chbAnnounce);
            this.metrotpRules.Controls.Add(this.btnSave);
            this.metrotpRules.Controls.Add(this.tbRules);
            this.metrotpRules.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.metrotpRules.HorizontalScrollbarBarColor = true;
            this.metrotpRules.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpRules.HorizontalScrollbarSize = 10;
            this.metrotpRules.Location = new System.Drawing.Point(4, 38);
            this.metrotpRules.Name = "metrotpRules";
            this.metrotpRules.Size = new System.Drawing.Size(886, 503);
            this.metrotpRules.TabIndex = 1;
            this.metrotpRules.Text = "Rules";
            this.metrotpRules.UseCustomBackColor = true;
            this.metrotpRules.VerticalScrollbarBarColor = true;
            this.metrotpRules.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpRules.VerticalScrollbarSize = 10;
            // 
            // metrotpMessages
            // 
            this.metrotpMessages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpMessages.Controls.Add(this.btnClearSelected);
            this.metrotpMessages.Controls.Add(this.pnlMessages);
            this.metrotpMessages.HorizontalScrollbarBarColor = true;
            this.metrotpMessages.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpMessages.HorizontalScrollbarSize = 10;
            this.metrotpMessages.Location = new System.Drawing.Point(4, 38);
            this.metrotpMessages.Name = "metrotpMessages";
            this.metrotpMessages.Size = new System.Drawing.Size(886, 503);
            this.metrotpMessages.TabIndex = 2;
            this.metrotpMessages.Text = "Messages";
            this.metrotpMessages.UseCustomBackColor = true;
            this.metrotpMessages.VerticalScrollbarBarColor = true;
            this.metrotpMessages.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpMessages.VerticalScrollbarSize = 10;
            // 
            // metrotpAnnouncements
            // 
            this.metrotpAnnouncements.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpAnnouncements.Controls.Add(this.dtpEndTimeAnn);
            this.metrotpAnnouncements.Controls.Add(this.dtpEndDateAnn);
            this.metrotpAnnouncements.Controls.Add(this.lblEndDate);
            this.metrotpAnnouncements.Controls.Add(this.lblEndTime);
            this.metrotpAnnouncements.Controls.Add(this.dtpStartTimeAnn);
            this.metrotpAnnouncements.Controls.Add(this.dtpStartDateAnn);
            this.metrotpAnnouncements.Controls.Add(this.cbMarkAsEvent);
            this.metrotpAnnouncements.Controls.Add(this.lblDate);
            this.metrotpAnnouncements.Controls.Add(this.lblTime);
            this.metrotpAnnouncements.Controls.Add(this.tbAnnouncementbody);
            this.metrotpAnnouncements.Controls.Add(this.btnSendAnnouncement);
            this.metrotpAnnouncements.HorizontalScrollbarBarColor = true;
            this.metrotpAnnouncements.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpAnnouncements.HorizontalScrollbarSize = 10;
            this.metrotpAnnouncements.Location = new System.Drawing.Point(4, 38);
            this.metrotpAnnouncements.Name = "metrotpAnnouncements";
            this.metrotpAnnouncements.Size = new System.Drawing.Size(886, 503);
            this.metrotpAnnouncements.TabIndex = 3;
            this.metrotpAnnouncements.Text = "Announcements";
            this.metrotpAnnouncements.UseCustomBackColor = true;
            this.metrotpAnnouncements.VerticalScrollbarBarColor = true;
            this.metrotpAnnouncements.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpAnnouncements.VerticalScrollbarSize = 10;
            // 
            // dtpEndTimeAnn
            // 
            this.dtpEndTimeAnn.CustomFormat = "HH:mm";
            this.dtpEndTimeAnn.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEndTimeAnn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTimeAnn.Location = new System.Drawing.Point(280, 438);
            this.dtpEndTimeAnn.Name = "dtpEndTimeAnn";
            this.dtpEndTimeAnn.ShowUpDown = true;
            this.dtpEndTimeAnn.Size = new System.Drawing.Size(112, 20);
            this.dtpEndTimeAnn.TabIndex = 10;
            this.dtpEndTimeAnn.Visible = false;
            // 
            // dtpEndDateAnn
            // 
            this.dtpEndDateAnn.CustomFormat = "yyyy-MM-dd";
            this.dtpEndDateAnn.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEndDateAnn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDateAnn.Location = new System.Drawing.Point(82, 440);
            this.dtpEndDateAnn.Name = "dtpEndDateAnn";
            this.dtpEndDateAnn.Size = new System.Drawing.Size(112, 20);
            this.dtpEndDateAnn.TabIndex = 11;
            this.dtpEndDateAnn.Visible = false;
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.lblEndDate.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEndDate.Location = new System.Drawing.Point(5, 440);
            this.lblEndDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(74, 19);
            this.lblEndDate.TabIndex = 8;
            this.lblEndDate.Text = "End date";
            this.lblEndDate.Visible = false;
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.lblEndTime.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEndTime.Location = new System.Drawing.Point(214, 439);
            this.lblEndTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(70, 19);
            this.lblEndTime.TabIndex = 9;
            this.lblEndTime.Text = "End time";
            this.lblEndTime.Visible = false;
            // 
            // dtpStartTimeAnn
            // 
            this.dtpStartTimeAnn.CustomFormat = "HH:mm";
            this.dtpStartTimeAnn.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStartTimeAnn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTimeAnn.Location = new System.Drawing.Point(280, 407);
            this.dtpStartTimeAnn.Name = "dtpStartTimeAnn";
            this.dtpStartTimeAnn.ShowUpDown = true;
            this.dtpStartTimeAnn.Size = new System.Drawing.Size(112, 20);
            this.dtpStartTimeAnn.TabIndex = 7;
            this.dtpStartTimeAnn.Visible = false;
            // 
            // dtpStartDateAnn
            // 
            this.dtpStartDateAnn.CustomFormat = "yyyy-MM-dd";
            this.dtpStartDateAnn.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpStartDateAnn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDateAnn.Location = new System.Drawing.Point(82, 407);
            this.dtpStartDateAnn.Name = "dtpStartDateAnn";
            this.dtpStartDateAnn.Size = new System.Drawing.Size(112, 20);
            this.dtpStartDateAnn.TabIndex = 7;
            this.dtpStartDateAnn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Century Gothic", 10.2F);
            this.btnExit.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExit.Location = new System.Drawing.Point(818, 591);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(68, 38);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // AgentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(894, 638);
            this.Controls.Add(this.metrotbMain);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AgentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AgentForm";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AgentForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.AgentForm_MouseMove);
            this.metrotbMain.ResumeLayout(false);
            this.metrotpResidents.ResumeLayout(false);
            this.metrotpResidents.PerformLayout();
            this.metrotpRules.ResumeLayout(false);
            this.metrotpRules.PerformLayout();
            this.metrotpMessages.ResumeLayout(false);
            this.metrotpAnnouncements.ResumeLayout(false);
            this.metrotpAnnouncements.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblRoomNo;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPhoneNo;
        private System.Windows.Forms.TextBox tbEmailAddress;
        private System.Windows.Forms.TextBox tbTelNo;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.ListBox lbxResidentsInfo;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.CheckBox chbAnnounce;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbRules;
        private System.Windows.Forms.Panel pnlMessages;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.CheckBox cbMarkAsEvent;
        private System.Windows.Forms.Button btnSendAnnouncement;
        private System.Windows.Forms.TextBox tbAnnouncementbody;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnClearSelected;
        private MetroFramework.Controls.MetroTabControl metrotbMain;
        private MetroFramework.Controls.MetroTabPage metrotpResidents;
        private MetroFramework.Controls.MetroTabPage metrotpRules;
        private MetroFramework.Controls.MetroTabPage metrotpMessages;
        private MetroFramework.Controls.MetroTabPage metrotpAnnouncements;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblLeaveDate;
        private System.Windows.Forms.Label lblEndContractDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Button btnShowUserDetails;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cbxRoomNo;
        private System.Windows.Forms.DateTimePicker dtpStartDateAnn;
        private System.Windows.Forms.DateTimePicker dtpStartTimeAnn;
        private System.Windows.Forms.DateTimePicker dtpLeaveDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Button btnRep;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.DateTimePicker dtpEndTimeAnn;
        private System.Windows.Forms.DateTimePicker dtpEndDateAnn;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.Label lblEndTime;
        private System.Windows.Forms.Button btnClear;
    }
}