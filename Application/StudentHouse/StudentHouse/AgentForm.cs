﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentHouse
{
    public partial class AgentForm : Form
    {
        string username;
        string password;
        DataTable messages;
        DataTable AllResidents;
        DataTable AllUsers;

        public AgentForm()
        {
            InitializeComponent();

            metrotbMain.SelectedTab = metrotpResidents;

            this.InitialState();

        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbFirstName.Text))
            {
                MessageBox.Show("Please Enter FirstName", "Error");
                return;
            }
            if (string.IsNullOrEmpty(tbLastName.Text))
            {
                MessageBox.Show("Please Enter LastName", "Error");
                return;
            }
            if (string.IsNullOrEmpty(cbxRoomNo.Text))
            {
                MessageBox.Show("Please Select a room number", "Error");
                return;
            }


            string firstNameResident = tbFirstName.Text;
            string lastNameResident = tbLastName.Text;
            byte roomNumber = Convert.ToByte(cbxRoomNo.Text);
            Resident addingNewResident = new Resident(roomNumber, firstNameResident, lastNameResident);
            if (!string.IsNullOrEmpty(tbTelNo.Text))
            {
                addingNewResident.SetPhone(tbTelNo.Text);
            }
            if (!string.IsNullOrEmpty(tbEmailAddress.Text))
            {
                addingNewResident.SetEmail(tbEmailAddress.Text);
            }

            addingNewResident.SetBalance(5);
            if (!string.IsNullOrEmpty(dtpStartDate.Value.ToString()) && !string.IsNullOrEmpty(dtpEndDate.Value.ToString()))
            {
                addingNewResident.SetDates(dtpStartDate.Value.ToString("yyyy-MM-dd")
                                            , dtpEndDate.Value.ToString("yyyy-MM-dd"));

            }
            addingNewResident.InsertResident();
            User assigningUserInfo = new User(addingNewResident);
            username = assigningUserInfo.GetUserInfo()["username"].ToString();
            password = assigningUserInfo.GetUserInfo()["password"].ToString();

            MessageBox.Show("Resident Created Successfully, Please Check User Details", "Resident Created Successfully");
            this.InitialState();
            btnShowUserDetails.Enabled = true;


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (lbxResidentsInfo.SelectedIndex != -1)
            {
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex].BeginEdit();
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["FirstName"] = tbFirstName.Text;
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["LastName"] = tbLastName.Text;
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Phone"] = tbTelNo.Text;
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Email"] = tbEmailAddress.Text;
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["StartDate"] = dtpStartDate.Value.ToString("yyyy-MM-dd");
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Expected_EndDate"] = dtpEndDate.Value.ToString("yyyy-MM-dd");
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["EndDate"] = dtpLeaveDate.Value.ToString("yyyy-MM-dd");
                AllResidents.Rows[lbxResidentsInfo.SelectedIndex].EndEdit();

                Resident resident = new Resident(AllResidents.Rows[lbxResidentsInfo.SelectedIndex]);
                resident.UpdateResident();
                MessageBox.Show("Resident details updated successfully");
                this.InitialState();
            }
            else
            {
                MessageBox.Show("Please select a resident from the list");
            }
        }

        private void btnShowUserDetails_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Log in: " + username + " \nPassword: " + password, "Resident access information");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbRules.Text))
            {
                Database database = new Database();
                database.ExecuteNonQuery("UPDATE rules SET body = '" + tbRules.Text + "'");
                tbRules.Text = database.GetData("SELECT body from rules").Rows[0][0].ToString();
                MessageBox.Show("Rules Updated");
                if (chbAnnounce.Checked)
                    metrotbMain.SelectedTab = metrotpAnnouncements;
            }
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginForm login = new LoginForm();
            login.Show();
        }

        private void metrotbMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (metrotbMain.SelectedTab == metrotpRules)
            {
                Database database = new Database();
                tbRules.Text = database.GetData("SELECT body from rules").Rows[0][0].ToString();
            }
            else if (metrotbMain.SelectedTab == metrotpMessages)
            {
                Panel pnl;
                CheckBox cb;
                TextBox tb;
                messages = Message.GetMessages();
                int heightofpanels = 10;
                foreach (DataRow row in messages.Rows)
                {
                    pnl = new Panel();
                    pnl.Name = "messagepnl_" + row["ID"].ToString();
                    cb = new CheckBox();
                    cb.Name = "messagecb_" + row["ID"].ToString();
                    tb = new TextBox();
                    if (row["sender_id"].ToString() != "-1")
                    {
                        string res_id = AllUsers.Select("ID = " + row["sender_id"].ToString())[0]["resident_id"].ToString();
                        string sender_FName = AllResidents.Select("ID = " + res_id)[0]["FirstName"].ToString();
                        string sender_LName = AllResidents.Select("ID = " + res_id)[0]["LastName"].ToString();
                        tb.Text = sender_FName + " " + sender_LName + ": " + row["body"].ToString();
                    }
                    else
                    {
                        tb.Text = row["body"].ToString();
                    }

                    int msgLength = tb.Lines.Where(line => !String.IsNullOrWhiteSpace(line)).Count();

                    pnl.Parent = pnlMessages;
                    pnl.Width = pnlMessages.Size.Width - 35;
                    pnl.Height = msgLength + 150;
                    pnl.BackColor = Color.Transparent;
                    pnl.Left = 7;
                    pnl.Top = heightofpanels;
                    pnl.BorderStyle = BorderStyle.FixedSingle;
                    heightofpanels += pnl.Height + 20;

                    cb.Parent = pnl;
                    cb.Text = "Select to delete";
                    cb.Top = msgLength + 125;
                    cb.Width = 120;
                    cb.Left = pnl.Size.Width - (cb.Size.Width + 10);
                    cb.TextAlign = ContentAlignment.MiddleCenter;
                    cb.Font = new Font("Century Gothic", 8, FontStyle.Regular);
                    cb.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    cb.CheckedChanged += new System.EventHandler(Cb_CheckedChanged);

                    tb.Parent = pnl;
                    tb.Multiline = true;
                    tb.Width = pnl.Size.Width - 20;
                    tb.Height = msgLength + 110;
                    tb.Top = 10;
                    tb.Left = 10;
                    tb.Font = new Font("Century Gothic", 9, FontStyle.Regular);
                    tb.ForeColor = Color.FromArgb(41, 39, 40);
                    tb.TextAlign = HorizontalAlignment.Left;
                    tb.BackColor = Color.FromKnownColor(KnownColor.ButtonFace); ;
                    tb.BorderStyle = BorderStyle.FixedSingle;
                }
            }
        }

        private void Cb_CheckedChanged(object sender, EventArgs e)
        {
            string ID = (sender as CheckBox).Name.Split('_')[1];
            messages.Select("ID = " + ID)[0]["read_flag"] = (sender as CheckBox).Checked;
        }

        private void btnClearSelected_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable updated_messages = messages.GetChanges(DataRowState.Modified);
                Message.UpdateMessages(updated_messages);
                pnlMessages.Controls.Clear();
                metrotbMain_SelectedIndexChanged(null, null);


            }
            catch (Exception er)
            {
                MessageBox.Show(er.Message);
            }
        }

        #region MouseFunctions
        Point lastClick; //Holds where the Form was clicked
        private void AgentForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void AgentForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }
        #endregion MouseFunctions
        private void btnSendAnnouncement_Click(object sender, EventArgs e)
        {
            string title = "Announcement From Agent";
            if (cbMarkAsEvent.Checked)
            {
                Event newEvent = new Event(5, -2);
                string ann_startTime = dtpStartTimeAnn.Value.ToString("HH:mm:ss");
                string ann_startDate = dtpStartDateAnn.Value.ToString("yyyy-MM-dd");
                string ann_endDate = dtpEndDateAnn.Value.ToString("yyyy-MM-dd");
                string ann_endTime = dtpEndTimeAnn.Value.ToString("HH:mm:ss");
                newEvent.SetDateTime(ann_startDate, ann_startTime, ann_endDate, ann_endTime);
                string errmsg = newEvent.InsertEvent();
                if (!string.IsNullOrEmpty(errmsg))
                {
                    MessageBox.Show(errmsg);
                }
                else
                {
                    MessageBox.Show("Announcement and event created successfully");
                    string body = tbAnnouncementbody.Text;
                    body += "\n" + ann_startDate + "|" + ann_startTime + " until " + ann_endDate + "|" + ann_endTime;
                    Announcement.SendAnnouncementToAllUsers(-2, title, body);
                    tbAnnouncementbody.Clear();
                }
            }
            else
            {
                string body = tbAnnouncementbody.Text;
                Announcement.SendAnnouncementToAllUsers(-2, title, body);
                MessageBox.Show("Announcement created successfully");
                tbAnnouncementbody.Clear();
            }
        }

        private void cbMarkAsEvent_CheckedChanged(object sender, EventArgs e)
        {
            lblDate.Visible = cbMarkAsEvent.Checked;
            lblTime.Visible = cbMarkAsEvent.Checked;
            lblEndDate.Visible = cbMarkAsEvent.Checked;
            lblEndTime.Visible = cbMarkAsEvent.Checked;
            dtpStartDateAnn.Visible = cbMarkAsEvent.Checked;
            dtpStartTimeAnn.Visible = cbMarkAsEvent.Checked;
            dtpEndDateAnn.Visible = cbMarkAsEvent.Checked;
            dtpEndTimeAnn.Visible = cbMarkAsEvent.Checked;
        }

        private void lbxResidentsInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxResidentsInfo.SelectedIndex != -1)
            {

                DataRow user_row = AllUsers.Select("Resident_id = " +
                    AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["ID"].ToString())[0];
                username = user_row["username"].ToString();
                password = user_row["password"].ToString();
                tbFirstName.Text = AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["FirstName"].ToString();
                tbLastName.Text = AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["LastName"].ToString();
                tbTelNo.Text = AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Phone"].ToString();
                tbEmailAddress.Text = AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Email"].ToString();
                dtpStartDate.Value = DateTime.Parse(AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["StartDate"].ToString());
                dtpEndDate.Value = DateTime.Parse(AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["Expected_EndDate"].ToString());
                if (!string.IsNullOrEmpty(AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["EndDate"].ToString()))
                    dtpLeaveDate.Value = DateTime.Parse(AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["EndDate"].ToString());
                cbxRoomNo.SelectedItem = AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["roomNumber"].ToString();

                btnShowUserDetails.Enabled = true;
                btnUpdate.Enabled = true;
                btnRep.Enabled = true;
                btnRemove.Enabled = true;
            }
        }

        private void btnRep_Click(object sender, EventArgs e)
        {
            if (lbxResidentsInfo.SelectedIndex != -1)
            {
                DataRow user_row = AllUsers.Select("Resident_id = " +
                    AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["ID"].ToString())[0];
                User updated_user = new User(user_row);
                if (Convert.ToByte(user_row["typeofuser"]) == 2)
                {
                    MessageBox.Show("This user already has been set as a Rep");
                    return;
                }
                updated_user.SetAsRep();
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbxResidentsInfo.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a resident from list");
                return;
            }
            DataRow resident_row = AllResidents.Select("ID = " +
                    AllResidents.Rows[lbxResidentsInfo.SelectedIndex]["ID"].ToString())[0];
            Resident deleteResident = new Resident(resident_row);
            string errmsg = deleteResident.DeleteResident();

            if (errmsg != string.Empty)
            {
                MessageBox.Show(errmsg);
                return;
            }
            MessageBox.Show("Resident removed successfully");
            AllResidents.Rows.Remove(resident_row);
            lbxResidentsInfo.Items.RemoveAt(lbxResidentsInfo.SelectedIndex);
            this.InitialState();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.InitialState();
        }
        private void InitialState()
        {
            AllResidents = Resident.GetList_Residents();
            AllUsers = User.GetAllUsers();
            lbxResidentsInfo.Items.Clear();
            foreach (DataRow row in AllResidents.Rows)
            {
                lbxResidentsInfo.Items.Add(row["FirstName"].ToString() + " " + row["LastName"].ToString() + " (" + row["roomNumber"].ToString() + ") ");
            }
            lbxResidentsInfo.SelectedIndex = -1;
            btnUpdate.Enabled = false;
            btnRemove.Enabled = false;
            btnRep.Enabled = false;
            btnShowUserDetails.Enabled = false;
            tbFirstName.Clear();
            tbLastName.Clear();
            tbEmailAddress.Clear();
            tbTelNo.Clear();
            dtpStartDate.Value = DateTime.Now;
            dtpEndDate.Value = DateTime.Now.AddYears(1);

            List<byte> rooms = new List<byte> { 11, 12, 13, 21, 22, 23, 31, 32 };
            foreach (DataRow row in Resident.TakenRooms().Rows)
            {
                rooms.Remove(Convert.ToByte(row[0]));
            }
            if (rooms.Count == 0)
            {
                cbxRoomNo.Text = string.Empty;
                cbxRoomNo.Enabled = false;
            }
            else
            {
                cbxRoomNo.DataSource = rooms;
                cbxRoomNo.Enabled = Enabled;
            }
        }
    }
}
