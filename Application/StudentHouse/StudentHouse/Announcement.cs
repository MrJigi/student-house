﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;

namespace StudentHouse
{
    public class Announcement
    {
        private int ID;
        private int recipient_user;
        private string title;
        private string body;
        private bool archived_flag;
        private int created_by;
        private DateTime created_at;
        private DateTime updated_at;

        private static Database database = new Database();

        public string Title 
        {
            get { return this.title; }
            set { this.title = value; }
        }

        public string Body
        {
            get { return this.body; }
            set { this.body = value; }
        }
        public Announcement(int create_user_id, int recipient_user_id)
        {
            this.ID = database.GetLastID("Announcements", string.Empty) + 1;
            this.recipient_user = recipient_user_id;
            this.title = string.Empty;
            this.body = string.Empty;
            this.archived_flag = false;
            this.created_by = create_user_id;
        }

        public string InsertAnnouncement()
        {
            
            if (this.CheckInsertConditions() != string.Empty)
            {
                return this.CheckInsertConditions();
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Announcement).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Announcements");
            return string.Empty;
        }

        public static void Archive(string announcement_id)
        {
            database.ExecuteNonQuery("Update Announcements SET archived_flag = 1 WHERE ID = " + announcement_id);
        }
        public static void SendAnnouncementToAllUsers(int user_id, string ptitle, string pbody)
        {
            DataTable dt_allusers = User.GetAllUsers();
            foreach (DataRow row in dt_allusers.Rows)
            {
                Announcement public_announcement = new Announcement(user_id, Convert.ToInt32(row["ID"]));
                public_announcement.Body = pbody;
                public_announcement.Title = ptitle;
                public_announcement.InsertAnnouncement();
            }
            
        }
        private string CheckInsertConditions()
        {
            return string.Empty;
        }
        private string CheckUpdateConditions()
        {
            return string.Empty;
        }
        private string CheckDeleteConditions()
        {
            return string.Empty;
        }

        public static DataTable GetAllAnnouncements(int recipient_user_id)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("SELECT * FROM Announcements WHERE archived_flag = 0");
            query.AppendLine("AND recipient_user = " + recipient_user_id.ToString());
            return database.GetData(query.ToString());
        }
    }
}
