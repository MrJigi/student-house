﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;


namespace StudentHouse
{
    class Database
    {
        private string ConnectionString;
        private MySqlConnection sqlConnection;
        public Database()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["sqlconnection"].ConnectionString;
            this.sqlConnection = new MySqlConnection(ConnectionString);
        }
        public DataTable GetData(string query)
        {
            DataTable data = new DataTable();

            if (!string.IsNullOrEmpty(query))
            {
                MySqlCommand command = new MySqlCommand(query, this.sqlConnection);
                this.sqlConnection.Open();

                MySqlDataReader dataReader = command.ExecuteReader();
                data.Load(dataReader);
                this.sqlConnection.Close();
            }
            return data;
        }

        public int GetLastID(string tableName, string Where)
        {
            DataTable data = new DataTable();

            if (!string.IsNullOrEmpty(tableName))
            {
                StringBuilder query = new StringBuilder("SELECT MAX(ID) FROM " + tableName + " WHERE 1=1 ");
                if (!string.IsNullOrEmpty(Where))
                    query.AppendLine(" AND " + Where);
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();

                MySqlDataReader dataReader = command.ExecuteReader();
                data.Load(dataReader);
                this.sqlConnection.Close();
            }
            if (data != null && data.Rows.Count > 0 && data.Rows[0][0] != DBNull.Value && Convert.ToInt32(data.Rows[0][0]) > 0)
            {
                return Convert.ToInt32(data.Rows[0][0]);
            }
            else
            {
                return 0;
            }

        }

        public void InsertData(DataTable data)
        {
            StringBuilder query = new StringBuilder();
            foreach (DataRow row in data.Rows)
            {
                query.AppendLine("INSERT INTO " + data.TableName + " (");
                foreach (DataColumn column in data.Columns)
                {
                    query.Append(column.ColumnName + ",");
                }
                query = query.Remove(query.Length - 1, 1);
                query.Append(")");
                query.AppendLine("VALUES (");
                foreach (DataColumn column in data.Columns)
                {
                    if (column.DataType != typeof(string))
                    {
                        query.Append(row[column] + ",");
                    }
                    else
                    {
                        query.Append("'" + row[column] + "'" + ",");
                    }
                }
                query = query.Remove(query.Length - 1, 1);
                query.AppendLine(");");
            }
            if (!string.IsNullOrEmpty(query.ToString()))
            {
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();
                command.ExecuteNonQuery();
                this.sqlConnection.Close();
            }

        }
        public void InsertData(Hashtable data, string tableName)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("INSERT INTO " + tableName + " (");
            foreach (var key in data.Keys)
            {
                query.Append(key.ToString() + ",");
            }

            query = query.Remove(query.Length - 1, 1);
            query.Append(")");
            query.AppendLine("VALUES (");
            foreach (var key in data.Keys)
            {
                if (data[key].GetType() != typeof(string))
                {
                    query.Append(data[key].ToString() + ",");
                }
                else
                {
                    query.Append("'" + data[key].ToString() + "'" + ",");
                }
            }
            query = query.Remove(query.Length - 1, 1);
            query.Append(")");

            if (!string.IsNullOrEmpty(query.ToString()))
            {
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();
                command.ExecuteNonQuery();
                this.sqlConnection.Close();
            }
        }

        public void InsertData(string query)
        {
            if (!string.IsNullOrEmpty(query.ToString()))
            {
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();
                command.ExecuteNonQuery();
                this.sqlConnection.Close();
            }
        }
        public void UpdateData(DataTable data, string Where = "")
        {   
            if (data.Columns.Contains("created_at"))
                data.Columns.Remove("created_at");
            foreach (DataRow row in data.Rows)
            {
                Hashtable updated_data = new Hashtable();
                foreach (DataColumn column in data.Columns)
                {
                    switch (column.DataType.Name)
                    {
                        case "DateTime":
                            updated_data.Add(column.ColumnName, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            break;
                        case "Boolean":
                            updated_data.Add(column.ColumnName, row[column]);
                            break;
                        default:
                            updated_data.Add(column.ColumnName, row[column]);
                            break;
                    }   
                }
                this.UpdateData(updated_data, data.TableName, Where);
            }
        }

        public void UpdateData(Hashtable data, string tableName, string Where = "")
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("UPDATE " + tableName + " SET ");
            foreach (var key in data.Keys)
            {
                query.Append(key.ToString() + "=");

                if (data[key].GetType() != typeof(string))
                {
                    query.Append(data[key].ToString() + ",");
                }
                else
                {
                    query.Append("'" + data[key].ToString() + "'" + ",");
                }
            }

            query = query.Remove(query.Length - 1, 1);
            query.AppendLine();
            if (data.ContainsKey("ID"))
                query.AppendLine("WHERE " + tableName + ".ID = " + data["ID"].ToString());
            else
                query.AppendLine("WHERE " + Where);

            if (!string.IsNullOrEmpty(query.ToString()))
            {
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();
                command.ExecuteNonQuery();
                this.sqlConnection.Close();
            }
        }
        public void ExecuteNonQuery(string query)
        {
            if (!string.IsNullOrEmpty(query.ToString()))
            {
                MySqlCommand command = new MySqlCommand(query.ToString(), this.sqlConnection);
                this.sqlConnection.Open();
                command.ExecuteNonQuery();
                this.sqlConnection.Close();
            }
        }
    }
}
