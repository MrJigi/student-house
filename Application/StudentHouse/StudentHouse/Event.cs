﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;
using System.Globalization;

namespace StudentHouse
{
    public class Event
    {
        private int ID;
        private byte typeof_event;
        /*
        Birthday party:1
        Dinner party:2
        Small gathering:3
        Game night party:4
        Events from agent:5
        Cleaning:6
        Shopping:7
        */
        private int created_by;
        private byte responsible_room;
        private string StartDate;
        private string EndDate;
        private string StartTime;
        private string EndTime;
        private string created_at;
        private string updated_at;

        private static Database database = new Database();

        public Event(byte type_of_event, int host_id)
        {
            this.ID = database.GetLastID("Events", string.Empty) + 1;
            this.typeof_event = type_of_event;
            this.created_by = host_id;
            StringBuilder query = new StringBuilder();
            query.AppendLine("SELECT roomNumber FROM Residents WHERE ID IN(");
            query.AppendLine("SELECT Resident_id FROM Users WHERE ID = " + host_id.ToString() + ")");
            if (this.typeof_event != 5)
                this.responsible_room = Convert.ToByte(database.GetData(query.ToString()).Rows[0][0]);
            else
                this.responsible_room = 100;
        }
        public string InsertEvent()
        {
            string errmsg = this.CheckInsertConditions();
            if (errmsg != string.Empty)
            {
                return errmsg;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Event).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Events");
            return string.Empty;
        }

        public static DataTable GetEvents(string start, string end)
        {
            string query = "SELECT * FROM Events WHERE ";
            query += "StartDate >= '" + start + "' AND EndDate <= '" + end + "'";
            return database.GetData(query);
        }
        //Add some delete functions to delete events in case party cancelled
        public void SetDateTime(string start_date, string start_time, string end_date, string end_time)
        {
            this.StartDate = start_date;
            this.EndDate = end_date;
            this.StartTime = start_time;
            this.EndTime = end_time;
        }
        private string CheckInsertConditions()
        {
            if (string.IsNullOrEmpty(this.StartDate))
                return "StartDate can't be empty!";
            if (string.IsNullOrEmpty(this.StartTime))
                return "StartTime can't be empty!";
            if (string.IsNullOrEmpty(this.EndDate))
                return "EndDate can't be empty!";
            if (string.IsNullOrEmpty(this.EndTime))
                return "EndTime can't be empty!";
            if (DateTime.Parse(this.StartDate).CompareTo(DateTime.Parse(this.EndDate)) == 1)
                return "Start date can't be later than end date!";
            if (DateTime.Now.AddDays(3).CompareTo(DateTime.Parse(this.StartDate)) == 1)
                return "Start date must be three days from now!";
            if (DateTime.Parse(this.StartDate).CompareTo(DateTime.Parse(this.EndDate)) == 0)
            {
                if (DateTime.Parse(this.EndTime).CompareTo(DateTime.Parse(this.StartTime)) == -1)
                    return "Start time can't be later than end time!";
            }
            return string.Empty;
        }

        public static void GnerateChores(DateTime Start, DateTime End)
        {

            StringBuilder deletequery = new StringBuilder();
            deletequery.AppendLine("DELETE FROM Events WHERE typeof_event IN (6,7)");
            deletequery.AppendLine("AND StartDate >= '" + Start.ToString("yyyy-MM-dd") + "'");
            deletequery.AppendLine("AND EndDate <= '" + End.ToString("yyyy-MM-dd") + "'");
            database.ExecuteNonQuery(deletequery.ToString());

            DataTable data = new DataTable();
            data.TableName = "Events";
            data.Columns.Add("ID", typeof(int));
            data.Columns.Add("typeof_event", typeof(byte));
            data.Columns.Add("created_by", typeof(int));
            data.Columns.Add("responsible_room", typeof(byte));
            data.Columns.Add("StartDate", typeof(string));
            data.Columns.Add("EndDate", typeof(string));
            data.Columns.Add("StartTime", typeof(string));
            data.Columns.Add("EndTime", typeof(string));
            data.Columns.Add("created_at", typeof(string));
            data.Columns.Add("updated_at", typeof(string));

            int LastId = database.GetLastID("Events", string.Empty) == 0 ? 1 : database.GetLastID("Events", string.Empty);

            DataTable roomsTable = database.GetData("SELECT DISTINCT roomNumber FROM Residents WHERE ID NOT IN (-1,-2)");
            List<byte> roomsList = new List<byte>();
            foreach (DataRow row in roomsTable.Rows)
            {
                roomsList.Add(Convert.ToByte(row[0]));
            }
            while (Start.CompareTo(End) < 0)
            {
                if (Start.DayOfWeek == DayOfWeek.Thursday)
                {
                    DataRow row = data.NewRow();
                    row["ID"] = ++LastId;
                    row["typeof_event"] = 6;
                    row["created_by"] = -2;
                    byte prev_room = 0;
                    if (data.Select("typeof_event = 6 ", "ID DESC").Length > 0)
                        prev_room = Convert.ToByte(data.Select("typeof_event = 6 ", "ID DESC")[0]["responsible_room"]);
                    else
                        prev_room = roomsList[0];
                    int cur_room = roomsList.FindIndex(item => item == prev_room) + 1 < roomsList.Count
                                 ? roomsList.FindIndex(item => item == prev_room) + 1 : 0;
                    row["responsible_room"] = roomsList[cur_room];
                    row["StartDate"] = Start.ToString("yyyy-MM-dd");
                    row["EndDate"] = Start.ToString("yyyy-MM-dd");
                    row["StartTime"] = "08:00:00";
                    row["EndTime"] = "16:00:00";
                    row["created_at"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    row["updated_at"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    data.Rows.Add(row);
                }
                if (Start.DayOfWeek == DayOfWeek.Friday)
                {
                    DataRow row = data.NewRow();
                    row["ID"] = ++LastId;
                    row["typeof_event"] = 7;
                    row["created_by"] = -2;
                    byte prev_room = 0;
                    if (data.Select("typeof_event = 7 ", "ID DESC").Length > 0)
                        prev_room = Convert.ToByte(data.Select("typeof_event = 7 ", "ID DESC")[0]["responsible_room"]);
                    else
                        prev_room = roomsList[roomsList.Count - 1];
                    int cur_room = roomsList.FindIndex(item => item == prev_room) + 1 < roomsList.Count
                                 ? roomsList.FindIndex(item => item == prev_room) + 1 : 0;
                    row["responsible_room"] = roomsList[cur_room];
                    row["StartDate"] = Start.ToString("yyyy-MM-dd");
                    row["EndDate"] = Start.ToString("yyyy-MM-dd");
                    row["StartTime"] = "08:00:00";
                    row["EndTime"] = "16:00:00";
                    row["created_at"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    row["updated_at"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    data.Rows.Add(row);
                }
                Start = Start.AddDays(1);
            }
            if (data.Rows.Count > 0)
            {
                database.InsertData(data);
            }
        }
        private void CheckUpdateConditions()
        {

        }
        private void CheckDeleteConditions()
        {

        }
    }
}
