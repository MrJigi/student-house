﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentHouse
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            
        }
        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (tbUsername.Text != string.Empty && tbPassword.Text != string.Empty)
            {
                User current_user = new User(tbUsername.Text, tbPassword.Text);
                if (current_user.Authenticate())
                {
                    if (Convert.ToByte(current_user.GetUserInfo()["typeofuser"]) != 3)
                    {
                        this.Hide();
                        ResidentForm RForm = new ResidentForm(current_user);
                        RForm.Show();
                    }
                    if (Convert.ToByte(current_user.GetUserInfo()["typeofuser"]) == 3)
                    {
                        this.Hide();
                        AgentForm AgForm = new AgentForm();
                        AgForm.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Invalid user credentials!");
                }

            }
        }
        Point lastClick; //Holds where the Form was clicked
        private void LoginForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void LoginForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        private void tbUsername_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
