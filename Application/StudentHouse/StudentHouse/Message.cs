﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;


namespace StudentHouse
{
    public class Message
    {
        private int ID;
        private int sender_id;
        private int reciever_id;
        private string body;
        private bool read_flag;
        private string created_at;
        private string updated_at;
        private static Database database = new Database();
        public Message(string body)
        {
            this.ID = database.GetLastID("Messages",string.Empty) + 1;
            this.sender_id = -1;
            this.reciever_id = -2;
            this.body = body;
        }
        public Message(int sender_id,string body)
        {
            this.ID = database.GetLastID("Messages", string.Empty) + 1;
            this.sender_id = sender_id;
            this.reciever_id = -2;
            this.body = body;
        }
        public Message(int sender_id, int reciever_id, string body)
        {
            this.ID = database.GetLastID("Messages", string.Empty) + 1;
            this.sender_id = sender_id;
            this.reciever_id = reciever_id;
            this.body = body;
        }
        public string InsertMessage()
        {
            if (this.CheckInsertConditions() != string.Empty)
            {
                return this.CheckInsertConditions();
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Message).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Messages");
            return string.Empty;
        }

        public static string UpdateMessages(DataTable data)
        {
            Hashtable updated_data = new Hashtable();
            data.TableName = "Messages";
            foreach (DataRow row in data.Rows)
            {
                if (row.RowState == DataRowState.Modified)
                {
                    if (!string.IsNullOrEmpty(CheckUpdateConditions(row)))
                    {
                        return CheckUpdateConditions(row);
                    }
                }
            }
            database.UpdateData(data);
            return string.Empty;
        }
        private string CheckInsertConditions()
        {
            return string.Empty;
        }
        private static string CheckUpdateConditions(DataRow row)
        {
            if (string.IsNullOrEmpty(row["body"].ToString()))
                return "body of message can't be empty!";
            
            return string.Empty;
        }

        public static DataTable GetMessages(string Where = "")
        {
            return database.GetData("SELECT ID, body, read_flag, updated_at,sender_id FROM MESSAGES WHERE read_flag = 0");
        }

    }


}
