﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace StudentHouse
{
    public class MoneyTransaction
    {
        private int paying_user;
        private int recipient_user;
        private double amount;
        private DateTime created_at = new DateTime();
        private DateTime updated_at = new DateTime();

        private static Database database = new Database();

        public MoneyTransaction(int paying_user, double amount)
        {
            this.paying_user = paying_user;
            this.amount = amount;
            this.recipient_user = -2;
        }
        public string InsertMoneyTransaction()
        {
            string errmsg = this.CheckInsertConditions();
            if (errmsg != string.Empty)
            {
                return errmsg;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(MoneyTransaction).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "MoneyTransactions");
            return string.Empty;
        }

        public static DataTable CheckPayment(int month)
        {
            StringBuilder query = new StringBuilder();
            query.AppendLine("SELECT FirstName, LastName, Balance FROM Residents WHERE ID IN (");
            query.AppendLine("SELECT Resident_id FROM Users WHERE ID IN ( ");
            query.AppendLine("SELECT paying_user WHERE created_id LIKE '%-"+ month.ToString() +"'");
            return new DataTable();
        }
        private void UpdateResidentBalance()
        {
            
        }

        private string CheckInsertConditions()
        {
            if (this.amount <= 0)
                return "Amount should be greater than zero!";
            return string.Empty;
        }


    }
}
