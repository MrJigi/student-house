﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;

namespace StudentHouse
{
    public class Purchase
    {
        private int ID;
        private int responsible_resident;
        private string purchased_items;
        private double totalprice;
        private string purchase_date;
        private string created_at;
        private string updated_at;

        private static Database database = new Database();

        public Purchase(int resident_id, string items, double price, string date = "")
        {
            this.ID = database.GetLastID("Purchases", string.Empty) + 1;
            this.responsible_resident = resident_id;
            this.purchased_items = items;
            this.totalprice = price;
            this.purchase_date = (string.IsNullOrWhiteSpace(date) || date == string.Empty) ?
                                 DateTime.Today.ToString("yyyy-MM-dd") : date;
        }

        public string InsertPurchase()
        {
            string result = this.CheckInsertConditions();
            if (result != string.Empty)
            {
                Console.WriteLine(this.CheckInsertConditions());
                return result;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Purchase).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Purchases");
            return string.Empty;
        }

        public static DataTable GetList_Purchases(string Where = "")
        {
            string query = "SELECT * FROM Purchases";
            query += (string.IsNullOrWhiteSpace(Where) || Where == string.Empty) ? string.Empty : (" WHERE " + Where);
            return database.GetData(query);
        }
        private string CheckInsertConditions()
        {
            return string.Empty;
        }
        private string CheckUpdateConditions()
        {
            return string.Empty;
        }
        private void CheckDeleteConditions()
        {

        }
    }
}
