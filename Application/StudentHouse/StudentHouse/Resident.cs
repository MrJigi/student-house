﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Reflection;
namespace StudentHouse
{
    public class Resident
    {
        private int ID;
        private byte building_id;
        private byte roomNumber;
        private string FirstName;
        private string LastName;
        private string Phone;
        private string Email;
        private string StartDate;
        private string Expected_EndDate;
        private string EndDate;
        private double Balance;
        private string created_at;
        private string updated_at;

        private static Database database = new Database();

        public Resident()
        {
            this.ID = database.GetLastID("Residents", string.Empty) + 1;
            this.building_id = 1;
            this.roomNumber = 0;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Phone = string.Empty;
            this.Email = string.Empty;
            this.StartDate = string.Empty;
            this.Expected_EndDate = string.Empty;
            this.EndDate = string.Empty;
            this.Balance = 0;
            this.created_at = string.Empty;
            this.updated_at = string.Empty;

        }
        public Resident(byte roomNumber, string firstname, string lastname)
        {
            this.ID = database.GetLastID("Residents", string.Empty) + 1;
            this.building_id = 1;
            this.roomNumber = roomNumber;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Phone = string.Empty;
            this.Email = string.Empty;
            this.StartDate = string.Empty;
            this.Expected_EndDate = string.Empty;
            this.EndDate = string.Empty;
            this.Balance = 0;
            this.created_at = string.Empty;
            this.updated_at = string.Empty;
        }
        public Resident(DataRow row)
        {
            this.ID = Convert.ToInt32(row["ID"]);
            this.building_id = Convert.ToByte(row["building_id"]);
            this.roomNumber = Convert.ToByte(row["roomNumber"]);
            this.FirstName = row["FirstName"].ToString();
            this.LastName = row["LastName"].ToString();
            this.Phone = row["Phone"].ToString();
            this.Email = row["Email"].ToString();
            this.StartDate = ((DateTime)row["StartDate"]).ToString("yyyy-MM-dd");
            this.Expected_EndDate = ((DateTime)row["Expected_EndDate"]).ToString("yyyy-MM-dd");
            
            if (string.IsNullOrEmpty(row["EndDate"].ToString()))
                this.EndDate = string.Empty;
            else
                this.EndDate = ((DateTime)row["EndDate"]).ToString("yyyy-MM-dd");

            this.Balance = Convert.ToDouble(row["Balance"]);
            this.created_at = ((DateTime)row["created_at"]).ToString("yyyy-MM-dd HH:mm:ss");
            this.updated_at = string.Empty;
        }

        /// <summary>
        /// This function adds current resident to the database. If there is any problem it will show the error inside the console
        /// </summary>
        public void InsertResident()
        {
            if (this.CheckInsertConditions() != string.Empty)
            {
                Console.WriteLine(this.CheckInsertConditions());
                return;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Resident).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Residents");
        }

        public void UpdateResident()
        {
            if (this.CheckUpdateConditions() != string.Empty)
            {
                Console.WriteLine(this.CheckUpdateConditions());
                return;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(Resident).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.UpdateData(data, "Residents");
        }

        public string DeleteResident()
        {
            string where_users = "SELECT ID FROM Users WHERE Resident_id = " + this.ID.ToString();
            if (this.CheckDeleteConditions() != string.Empty)
                return this.CheckDeleteConditions();
            database.ExecuteNonQuery("DELETE FROM Messages WHERE Reciever_id IN (" + where_users + ")");
            database.ExecuteNonQuery("DELETE FROM Announcements WHERE recipient_user IN (" + where_users + ")");
            database.ExecuteNonQuery("DELETE FROM Users WHERE resident_id = " + this.ID.ToString());
            database.ExecuteNonQuery("DELETE FROM Residents WHERE ID = " + this.ID.ToString());
            return string.Empty;
        }
        public void SetPhone(string phone)
        {
            this.Phone = phone;
        }
        public void SetEmail(string email)
        {
            this.Email = email;
        }
        public void SetBalance(double amount)
        {
            this.Balance = amount;
        }
        public void SetDates(string start, string expected, string end = "")
        {
            this.StartDate = start;
            this.Expected_EndDate = expected;
            this.EndDate = end;
        }
        /// <summary>
        /// Returnes current object Info in hashtable. Hashtable keys are instance variable of class.
        /// </summary>
        /// <returns></returns>
        public Hashtable GetResidentInfo()
        {
            Hashtable Info = new Hashtable();
            foreach (var field in typeof(Resident).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                Info.Add(field.Name, field.GetValue(this));

            }
            return Info;
        }
        public static DataTable GetList_Residents(string Where = "")
        {
            string query = "SELECT * FROM Residents WHERE ID NOT IN (-1,-2)";
            query += (string.IsNullOrWhiteSpace(Where) || Where == string.Empty) ? string.Empty : (" AND " + Where);
            return database.GetData(query);
        }

        public void IncreaseBalance(double amount)
        {
            this.Balance += amount;
            this.UpdateResident();
        }

        public static List<string> GenerateBlackList()
        {
            DataTable dt_listofResidents = Resident.GetList_Residents();
            List<string> blacklist = new List<string>();
            foreach(DataRow row in dt_listofResidents.Rows)
            {
                Resident selected_resident = new Resident(row);
                double balance = Convert.ToDouble(row["Balance"]);
                int first_month = ((DateTime)row["StartDate"]).Month;

                string[] months = { "January", "February", "March", "April", "May"
                                , "June","July","August","September", "October", "November", "December" };

                double fee = 5.00;
                int count_payed_months = Convert.ToInt32(balance / fee);
                int count_months_from_start = 0;
                if (((DateTime)row["StartDate"]).Year != DateTime.Today.Year)
                {
                    int years = DateTime.Today.Year - ((DateTime)row["StartDate"]).Year;
                    int start_year_months = 12 - (((DateTime)row["StartDate"]).Month);
                    int cur_year_month = DateTime.Today.Month;
                    if (years > 1)
                        count_months_from_start += (years - 1) * 12;
                    count_months_from_start += cur_year_month + start_year_months;
                }
                if (count_payed_months == 0 || count_months_from_start > count_payed_months)
                {
                    blacklist.Add(row["FirstName"].ToString() + " " + row["LastName"].ToString()
                                  +": " + (count_months_from_start-count_payed_months).ToString() + " month[s]" );
                }
            }
            return blacklist;
        }

        public static DataTable TakenRooms()
        {
            return database.GetData("SELECT roomNumber FROM Residents WHERE ID NOT IN (-1,-2)");
        }
        private string CheckInsertConditions()
        {
            if (string.IsNullOrEmpty(this.FirstName))
                return "Resident's FirstName can't be empty!";
            if (string.IsNullOrEmpty(this.LastName))
                return "Resident's LastName can't be empty!";
            if (string.IsNullOrEmpty(this.StartDate))
                return "Resident's StartDate can't be empty!";
            if (string.IsNullOrEmpty(this.Expected_EndDate))
                return "Resident's Expected_EndDate can't be empty!";
            if (DateTime.Parse(this.StartDate).CompareTo(DateTime.Parse(this.Expected_EndDate)) == 1)
                return "Start date can't be later than end of contract!";
            return string.Empty;
        }
        private string CheckUpdateConditions()
        {
            if (string.IsNullOrEmpty(this.FirstName))
                return "Resident's FirstName can't be empty!";
            if (string.IsNullOrEmpty(this.LastName))
                return "Resident's LastName can't be empty!";
            if (string.IsNullOrEmpty(this.StartDate))
                return "Resident's StartDate can't be empty!";
            if (string.IsNullOrEmpty(this.Expected_EndDate))
                return "Resident's Expected_EndDate can't be empty!";
            if (DateTime.Parse(this.StartDate).CompareTo(DateTime.Parse(this.Expected_EndDate)) == 1)
                return "Start date can't be later than end of contract!";
            return string.Empty;
        }
        private string CheckDeleteConditions()
        {
            string where_users = "SELECT ID FROM Users WHERE Resident_id = " + this.ID.ToString();
            if (database.GetData("SELECT 1 FROM Events WHERE created_by IN (" + where_users + ")").Rows.Count > 0)
                return "Delete action can't proceed. There are some event records created by this resident";
            if (database.GetData("SELECT 1 FROM Messages WHERE sender_id IN (" + where_users + ")").Rows.Count > 0)
                return "Delete action can't proceed. There are some message records created by this resident";
            if (database.GetData("SELECT 1 FROM Announcements WHERE created_by IN (" + where_users + ")").Rows.Count > 0)
                return "Delete action can't proceed. There are some registered announcement records created by this resident";
            if (database.GetData("SELECT 1 FROM Purchases WHERE responsible_resident = " + this.ID.ToString()).Rows.Count > 0)
                return "Delete action can't proceed. There are some purchases records done by this resident";

            return string.Empty;
        }

    }
}
