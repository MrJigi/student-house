﻿namespace StudentHouse
{
    partial class ResidentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResidentForm));
            this.pnlSide = new System.Windows.Forms.Panel();
            this.gbxBlackList = new System.Windows.Forms.GroupBox();
            this.lbxBlackList = new System.Windows.Forms.ListBox();
            this.pnlAnnouncement = new System.Windows.Forms.Panel();
            this.btnAboutUs = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.gbxReport = new System.Windows.Forms.GroupBox();
            this.cbxReportMonth = new System.Windows.Forms.ComboBox();
            this.lblMonth = new System.Windows.Forms.Label();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.metrotbcMain = new MetroFramework.Controls.MetroTabControl();
            this.metrotpAgenda = new MetroFramework.Controls.MetroTabPage();
            this.pbxLeftArrow = new System.Windows.Forms.PictureBox();
            this.pbxRightArrow = new System.Windows.Forms.PictureBox();
            this.lblWeek = new System.Windows.Forms.Label();
            this.btnGenerateChores = new System.Windows.Forms.Button();
            this.pnlAgenda = new System.Windows.Forms.Panel();
            this.metrotpCreateEvent = new MetroFramework.Controls.MetroTabPage();
            this.gbEnd = new System.Windows.Forms.GroupBox();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.lbEndTime = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.lbEndDate = new System.Windows.Forms.Label();
            this.btnCreateEvent = new System.Windows.Forms.Button();
            this.gbStart = new System.Windows.Forms.GroupBox();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lbStartTime = new System.Windows.Forms.Label();
            this.lbStartDate = new System.Windows.Forms.Label();
            this.lbHostname = new System.Windows.Forms.Label();
            this.lbHost = new System.Windows.Forms.Label();
            this.lbTypeofevent = new System.Windows.Forms.Label();
            this.cbTypeofevent = new System.Windows.Forms.ComboBox();
            this.metrotpReport = new MetroFramework.Controls.MetroTabPage();
            this.chbAnonymous = new System.Windows.Forms.CheckBox();
            this.tbMessageBody = new System.Windows.Forms.TextBox();
            this.lbMessage = new System.Windows.Forms.Label();
            this.metrotpBalance = new MetroFramework.Controls.MetroTabPage();
            this.rbtnPaymentMode = new System.Windows.Forms.RadioButton();
            this.rbtnPurchaseMode = new System.Windows.Forms.RadioButton();
            this.rbtnReportMode = new System.Windows.Forms.RadioButton();
            this.mtbPurchaseTotalPrice = new System.Windows.Forms.MaskedTextBox();
            this.btnPurchaseAddItems = new System.Windows.Forms.Button();
            this.lblPurchaseList = new System.Windows.Forms.Label();
            this.lblPurchaseTotalPrice = new System.Windows.Forms.Label();
            this.lbxBalance = new System.Windows.Forms.ListBox();
            this.gbxPayment = new System.Windows.Forms.GroupBox();
            this.mtbPaymentAmount = new System.Windows.Forms.MaskedTextBox();
            this.btnPaymentUpdateBalane = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cbxPaymentResident = new System.Windows.Forms.ComboBox();
            this.lbPaymentResident = new System.Windows.Forms.Label();
            this.gbxPurchase = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.tbPurchaseQuantity = new System.Windows.Forms.TextBox();
            this.tbPurchaseItem = new System.Windows.Forms.TextBox();
            this.btnPurchaseRemoveItem = new System.Windows.Forms.Button();
            this.btnPurchaseAddItem = new System.Windows.Forms.Button();
            this.cbxPurchaseResident = new System.Windows.Forms.ComboBox();
            this.dtpPurchesDate = new System.Windows.Forms.DateTimePicker();
            this.lbPurchaseQuantity = new System.Windows.Forms.Label();
            this.lbPurchaseItem = new System.Windows.Forms.Label();
            this.lbPurchaseResident = new System.Windows.Forms.Label();
            this.lbPurchaseDate = new System.Windows.Forms.Label();
            this.metrotpRules = new MetroFramework.Controls.MetroTabPage();
            this.tbRules = new System.Windows.Forms.TextBox();
            this.tmAnnouncements = new System.Windows.Forms.Timer(this.components);
            this.pnlSide.SuspendLayout();
            this.gbxBlackList.SuspendLayout();
            this.gbxReport.SuspendLayout();
            this.metrotbcMain.SuspendLayout();
            this.metrotpAgenda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLeftArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRightArrow)).BeginInit();
            this.metrotpCreateEvent.SuspendLayout();
            this.gbEnd.SuspendLayout();
            this.gbStart.SuspendLayout();
            this.metrotpReport.SuspendLayout();
            this.metrotpBalance.SuspendLayout();
            this.gbxPayment.SuspendLayout();
            this.gbxPurchase.SuspendLayout();
            this.metrotpRules.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSide
            // 
            this.pnlSide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.pnlSide.Controls.Add(this.gbxBlackList);
            this.pnlSide.Controls.Add(this.pnlAnnouncement);
            this.pnlSide.Controls.Add(this.btnAboutUs);
            this.pnlSide.Controls.Add(this.btnExit);
            this.pnlSide.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSide.Location = new System.Drawing.Point(805, 0);
            this.pnlSide.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlSide.Name = "pnlSide";
            this.pnlSide.Size = new System.Drawing.Size(387, 785);
            this.pnlSide.TabIndex = 0;
            // 
            // gbxBlackList
            // 
            this.gbxBlackList.Controls.Add(this.lbxBlackList);
            this.gbxBlackList.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.gbxBlackList.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxBlackList.Location = new System.Drawing.Point(19, 15);
            this.gbxBlackList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxBlackList.Name = "gbxBlackList";
            this.gbxBlackList.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxBlackList.Size = new System.Drawing.Size(336, 144);
            this.gbxBlackList.TabIndex = 5;
            this.gbxBlackList.TabStop = false;
            this.gbxBlackList.Text = "BlackList";
            // 
            // lbxBlackList
            // 
            this.lbxBlackList.FormattingEnabled = true;
            this.lbxBlackList.ItemHeight = 20;
            this.lbxBlackList.Location = new System.Drawing.Point(9, 27);
            this.lbxBlackList.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbxBlackList.Name = "lbxBlackList";
            this.lbxBlackList.Size = new System.Drawing.Size(317, 104);
            this.lbxBlackList.TabIndex = 0;
            // 
            // pnlAnnouncement
            // 
            this.pnlAnnouncement.AutoScroll = true;
            this.pnlAnnouncement.BackColor = System.Drawing.Color.Transparent;
            this.pnlAnnouncement.Location = new System.Drawing.Point(19, 180);
            this.pnlAnnouncement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlAnnouncement.Name = "pnlAnnouncement";
            this.pnlAnnouncement.Size = new System.Drawing.Size(341, 516);
            this.pnlAnnouncement.TabIndex = 4;
            this.pnlAnnouncement.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAnnouncement_Paint);
            // 
            // btnAboutUs
            // 
            this.btnAboutUs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnAboutUs.FlatAppearance.BorderSize = 0;
            this.btnAboutUs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAboutUs.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnAboutUs.ForeColor = System.Drawing.SystemColors.Window;
            this.btnAboutUs.Location = new System.Drawing.Point(164, 726);
            this.btnAboutUs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAboutUs.Name = "btnAboutUs";
            this.btnAboutUs.Size = new System.Drawing.Size(101, 46);
            this.btnAboutUs.TabIndex = 3;
            this.btnAboutUs.Text = "About Us";
            this.btnAboutUs.UseVisualStyleBackColor = false;
            this.btnAboutUs.Click += new System.EventHandler(this.btnAboutUs_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnExit.ForeColor = System.Drawing.SystemColors.Window;
            this.btnExit.Location = new System.Drawing.Point(271, 726);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(101, 46);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // gbxReport
            // 
            this.gbxReport.Controls.Add(this.cbxReportMonth);
            this.gbxReport.Controls.Add(this.lblMonth);
            this.gbxReport.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxReport.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxReport.Location = new System.Drawing.Point(11, 55);
            this.gbxReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxReport.Name = "gbxReport";
            this.gbxReport.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxReport.Size = new System.Drawing.Size(445, 71);
            this.gbxReport.TabIndex = 5;
            this.gbxReport.TabStop = false;
            this.gbxReport.Text = "Report";
            // 
            // cbxReportMonth
            // 
            this.cbxReportMonth.FormattingEnabled = true;
            this.cbxReportMonth.Location = new System.Drawing.Point(87, 26);
            this.cbxReportMonth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxReportMonth.Name = "cbxReportMonth";
            this.cbxReportMonth.Size = new System.Drawing.Size(160, 28);
            this.cbxReportMonth.TabIndex = 0;
            this.cbxReportMonth.SelectedIndexChanged += new System.EventHandler(this.cbxReportMonth_SelectedIndexChanged);
            // 
            // lblMonth
            // 
            this.lblMonth.AutoSize = true;
            this.lblMonth.Location = new System.Drawing.Point(12, 30);
            this.lblMonth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Size = new System.Drawing.Size(57, 20);
            this.lblMonth.TabIndex = 4;
            this.lblMonth.Text = "Month";
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnSendMessage.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.btnSendMessage.FlatAppearance.BorderSize = 0;
            this.btnSendMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSendMessage.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnSendMessage.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSendMessage.Location = new System.Drawing.Point(611, 341);
            this.btnSendMessage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(115, 46);
            this.btnSendMessage.TabIndex = 2;
            this.btnSendMessage.Text = "Send";
            this.btnSendMessage.UseVisualStyleBackColor = false;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(805, 50);
            this.panel2.TabIndex = 2;
            // 
            // metrotbcMain
            // 
            this.metrotbcMain.Controls.Add(this.metrotpAgenda);
            this.metrotbcMain.Controls.Add(this.metrotpCreateEvent);
            this.metrotbcMain.Controls.Add(this.metrotpReport);
            this.metrotbcMain.Controls.Add(this.metrotpBalance);
            this.metrotbcMain.Controls.Add(this.metrotpRules);
            this.metrotbcMain.ItemSize = new System.Drawing.Size(114, 34);
            this.metrotbcMain.Location = new System.Drawing.Point(0, 48);
            this.metrotbcMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotbcMain.Name = "metrotbcMain";
            this.metrotbcMain.SelectedIndex = 3;
            this.metrotbcMain.Size = new System.Drawing.Size(805, 737);
            this.metrotbcMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.metrotbcMain.Style = MetroFramework.MetroColorStyle.FourstandingGreen;
            this.metrotbcMain.TabIndex = 1;
            this.metrotbcMain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metrotbcMain.Theme = MetroFramework.MetroThemeStyle.Fourstanding;
            this.metrotbcMain.UseSelectable = true;
            this.metrotbcMain.SelectedIndexChanged += new System.EventHandler(this.metrotbcMain_SelectedIndexChanged);
            // 
            // metrotpAgenda
            // 
            this.metrotpAgenda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpAgenda.Controls.Add(this.pbxLeftArrow);
            this.metrotpAgenda.Controls.Add(this.pbxRightArrow);
            this.metrotpAgenda.Controls.Add(this.lblWeek);
            this.metrotpAgenda.Controls.Add(this.btnGenerateChores);
            this.metrotpAgenda.Controls.Add(this.pnlAgenda);
            this.metrotpAgenda.HorizontalScrollbarBarColor = true;
            this.metrotpAgenda.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpAgenda.HorizontalScrollbarSize = 12;
            this.metrotpAgenda.Location = new System.Drawing.Point(4, 38);
            this.metrotpAgenda.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotpAgenda.Name = "metrotpAgenda";
            this.metrotpAgenda.Size = new System.Drawing.Size(797, 695);
            this.metrotpAgenda.TabIndex = 0;
            this.metrotpAgenda.Text = "Agenda";
            this.metrotpAgenda.UseCustomBackColor = true;
            this.metrotpAgenda.VerticalScrollbarBarColor = true;
            this.metrotpAgenda.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpAgenda.VerticalScrollbarSize = 13;
            // 
            // pbxLeftArrow
            // 
            this.pbxLeftArrow.Image = global::StudentHouse.Properties.Resources.Larrow_icon;
            this.pbxLeftArrow.Location = new System.Drawing.Point(11, 27);
            this.pbxLeftArrow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbxLeftArrow.Name = "pbxLeftArrow";
            this.pbxLeftArrow.Size = new System.Drawing.Size(33, 38);
            this.pbxLeftArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxLeftArrow.TabIndex = 0;
            this.pbxLeftArrow.TabStop = false;
            this.pbxLeftArrow.Click += new System.EventHandler(this.pbxLeftArrow_Click);
            // 
            // pbxRightArrow
            // 
            this.pbxRightArrow.Image = global::StudentHouse.Properties.Resources.Rarrow_icon;
            this.pbxRightArrow.Location = new System.Drawing.Point(461, 27);
            this.pbxRightArrow.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbxRightArrow.Name = "pbxRightArrow";
            this.pbxRightArrow.Size = new System.Drawing.Size(35, 38);
            this.pbxRightArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxRightArrow.TabIndex = 5;
            this.pbxRightArrow.TabStop = false;
            this.pbxRightArrow.Click += new System.EventHandler(this.pbxRightArrow_Click);
            // 
            // lblWeek
            // 
            this.lblWeek.AutoSize = true;
            this.lblWeek.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeek.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblWeek.Location = new System.Drawing.Point(52, 31);
            this.lblWeek.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblWeek.Name = "lblWeek";
            this.lblWeek.Size = new System.Drawing.Size(89, 32);
            this.lblWeek.TabIndex = 4;
            this.lblWeek.Text = "Week";
            // 
            // btnGenerateChores
            // 
            this.btnGenerateChores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnGenerateChores.FlatAppearance.BorderSize = 0;
            this.btnGenerateChores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGenerateChores.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnGenerateChores.ForeColor = System.Drawing.SystemColors.Window;
            this.btnGenerateChores.Location = new System.Drawing.Point(588, 631);
            this.btnGenerateChores.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGenerateChores.Name = "btnGenerateChores";
            this.btnGenerateChores.Size = new System.Drawing.Size(203, 46);
            this.btnGenerateChores.TabIndex = 3;
            this.btnGenerateChores.Text = "Generate Chores";
            this.btnGenerateChores.UseVisualStyleBackColor = false;
            this.btnGenerateChores.Click += new System.EventHandler(this.btnGenerateChores_Click);
            // 
            // pnlAgenda
            // 
            this.pnlAgenda.AutoScroll = true;
            this.pnlAgenda.Location = new System.Drawing.Point(4, 76);
            this.pnlAgenda.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlAgenda.Name = "pnlAgenda";
            this.pnlAgenda.Size = new System.Drawing.Size(787, 524);
            this.pnlAgenda.TabIndex = 2;
            // 
            // metrotpCreateEvent
            // 
            this.metrotpCreateEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpCreateEvent.Controls.Add(this.gbEnd);
            this.metrotpCreateEvent.Controls.Add(this.btnCreateEvent);
            this.metrotpCreateEvent.Controls.Add(this.gbStart);
            this.metrotpCreateEvent.Controls.Add(this.lbHostname);
            this.metrotpCreateEvent.Controls.Add(this.lbHost);
            this.metrotpCreateEvent.Controls.Add(this.lbTypeofevent);
            this.metrotpCreateEvent.Controls.Add(this.cbTypeofevent);
            this.metrotpCreateEvent.HorizontalScrollbarBarColor = true;
            this.metrotpCreateEvent.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpCreateEvent.HorizontalScrollbarSize = 12;
            this.metrotpCreateEvent.Location = new System.Drawing.Point(4, 38);
            this.metrotpCreateEvent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotpCreateEvent.Name = "metrotpCreateEvent";
            this.metrotpCreateEvent.Size = new System.Drawing.Size(797, 695);
            this.metrotpCreateEvent.TabIndex = 1;
            this.metrotpCreateEvent.Text = "Create an event";
            this.metrotpCreateEvent.Theme = MetroFramework.MetroThemeStyle.Fourstanding;
            this.metrotpCreateEvent.VerticalScrollbarBarColor = true;
            this.metrotpCreateEvent.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpCreateEvent.VerticalScrollbarSize = 13;
            // 
            // gbEnd
            // 
            this.gbEnd.Controls.Add(this.dtpEndTime);
            this.gbEnd.Controls.Add(this.lbEndTime);
            this.gbEnd.Controls.Add(this.dtpEndDate);
            this.gbEnd.Controls.Add(this.lbEndDate);
            this.gbEnd.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.gbEnd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbEnd.Location = new System.Drawing.Point(45, 284);
            this.gbEnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEnd.Name = "gbEnd";
            this.gbEnd.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbEnd.Size = new System.Drawing.Size(596, 98);
            this.gbEnd.TabIndex = 17;
            this.gbEnd.TabStop = false;
            this.gbEnd.Text = "End";
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.CustomFormat = "HH:mm";
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndTime.Location = new System.Drawing.Point(313, 43);
            this.dtpEndTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(160, 23);
            this.dtpEndTime.TabIndex = 19;
            // 
            // lbEndTime
            // 
            this.lbEndTime.AutoSize = true;
            this.lbEndTime.Location = new System.Drawing.Point(309, 20);
            this.lbEndTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbEndTime.Name = "lbEndTime";
            this.lbEndTime.Size = new System.Drawing.Size(39, 19);
            this.lbEndTime.TabIndex = 3;
            this.lbEndTime.Text = "Time";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.CustomFormat = "dd-MM-yyyy";
            this.dtpEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEndDate.Location = new System.Drawing.Point(121, 43);
            this.dtpEndDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(160, 23);
            this.dtpEndDate.TabIndex = 18;
            // 
            // lbEndDate
            // 
            this.lbEndDate.AutoSize = true;
            this.lbEndDate.Location = new System.Drawing.Point(117, 20);
            this.lbEndDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbEndDate.Name = "lbEndDate";
            this.lbEndDate.Size = new System.Drawing.Size(43, 19);
            this.lbEndDate.TabIndex = 3;
            this.lbEndDate.Text = "Date";
            // 
            // btnCreateEvent
            // 
            this.btnCreateEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnCreateEvent.FlatAppearance.BorderSize = 0;
            this.btnCreateEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateEvent.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnCreateEvent.ForeColor = System.Drawing.SystemColors.Window;
            this.btnCreateEvent.Location = new System.Drawing.Point(540, 420);
            this.btnCreateEvent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCreateEvent.Name = "btnCreateEvent";
            this.btnCreateEvent.Size = new System.Drawing.Size(101, 46);
            this.btnCreateEvent.TabIndex = 3;
            this.btnCreateEvent.Text = "Create";
            this.btnCreateEvent.UseVisualStyleBackColor = false;
            this.btnCreateEvent.Click += new System.EventHandler(this.btnCreateEvent_Click);
            // 
            // gbStart
            // 
            this.gbStart.Controls.Add(this.dtpStartTime);
            this.gbStart.Controls.Add(this.dtpStartDate);
            this.gbStart.Controls.Add(this.lbStartTime);
            this.gbStart.Controls.Add(this.lbStartDate);
            this.gbStart.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.gbStart.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbStart.Location = new System.Drawing.Point(45, 161);
            this.gbStart.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbStart.Name = "gbStart";
            this.gbStart.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbStart.Size = new System.Drawing.Size(596, 98);
            this.gbStart.TabIndex = 4;
            this.gbStart.TabStop = false;
            this.gbStart.Text = "Start";
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.CustomFormat = "HH:mm";
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartTime.Location = new System.Drawing.Point(313, 43);
            this.dtpStartTime.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(160, 23);
            this.dtpStartTime.TabIndex = 19;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.CustomFormat = "dd-MM-yyyy";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(121, 43);
            this.dtpStartDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(160, 23);
            this.dtpStartDate.TabIndex = 18;
            // 
            // lbStartTime
            // 
            this.lbStartTime.AutoSize = true;
            this.lbStartTime.Location = new System.Drawing.Point(309, 20);
            this.lbStartTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbStartTime.Name = "lbStartTime";
            this.lbStartTime.Size = new System.Drawing.Size(39, 19);
            this.lbStartTime.TabIndex = 3;
            this.lbStartTime.Text = "Time";
            // 
            // lbStartDate
            // 
            this.lbStartDate.AutoSize = true;
            this.lbStartDate.Location = new System.Drawing.Point(117, 20);
            this.lbStartDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbStartDate.Name = "lbStartDate";
            this.lbStartDate.Size = new System.Drawing.Size(43, 19);
            this.lbStartDate.TabIndex = 3;
            this.lbStartDate.Text = "Date";
            // 
            // lbHostname
            // 
            this.lbHostname.AutoSize = true;
            this.lbHostname.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.lbHostname.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbHostname.Location = new System.Drawing.Point(163, 118);
            this.lbHostname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbHostname.Name = "lbHostname";
            this.lbHostname.Size = new System.Drawing.Size(79, 19);
            this.lbHostname.TabIndex = 3;
            this.lbHostname.Text = "Hostname";
            // 
            // lbHost
            // 
            this.lbHost.AutoSize = true;
            this.lbHost.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.lbHost.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbHost.Location = new System.Drawing.Point(41, 118);
            this.lbHost.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbHost.Name = "lbHost";
            this.lbHost.Size = new System.Drawing.Size(38, 19);
            this.lbHost.TabIndex = 3;
            this.lbHost.Text = "Host";
            // 
            // lbTypeofevent
            // 
            this.lbTypeofevent.AutoSize = true;
            this.lbTypeofevent.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.lbTypeofevent.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbTypeofevent.Location = new System.Drawing.Point(43, 64);
            this.lbTypeofevent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTypeofevent.Name = "lbTypeofevent";
            this.lbTypeofevent.Size = new System.Drawing.Size(101, 19);
            this.lbTypeofevent.TabIndex = 3;
            this.lbTypeofevent.Text = "Type of Event";
            // 
            // cbTypeofevent
            // 
            this.cbTypeofevent.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.cbTypeofevent.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTypeofevent.FormattingEnabled = true;
            this.cbTypeofevent.Items.AddRange(new object[] {
            "Birthday party",
            "Dinner party",
            "Small gathering",
            "Game night party"});
            this.cbTypeofevent.Location = new System.Drawing.Point(167, 60);
            this.cbTypeofevent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbTypeofevent.Name = "cbTypeofevent";
            this.cbTypeofevent.Size = new System.Drawing.Size(160, 28);
            this.cbTypeofevent.TabIndex = 2;
            // 
            // metrotpReport
            // 
            this.metrotpReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpReport.Controls.Add(this.chbAnonymous);
            this.metrotpReport.Controls.Add(this.tbMessageBody);
            this.metrotpReport.Controls.Add(this.btnSendMessage);
            this.metrotpReport.Controls.Add(this.lbMessage);
            this.metrotpReport.HorizontalScrollbarBarColor = true;
            this.metrotpReport.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpReport.HorizontalScrollbarSize = 12;
            this.metrotpReport.Location = new System.Drawing.Point(4, 38);
            this.metrotpReport.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotpReport.Name = "metrotpReport";
            this.metrotpReport.Size = new System.Drawing.Size(797, 695);
            this.metrotpReport.TabIndex = 2;
            this.metrotpReport.Text = "Report an issue";
            this.metrotpReport.UseCustomBackColor = true;
            this.metrotpReport.VerticalScrollbarBarColor = true;
            this.metrotpReport.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpReport.VerticalScrollbarSize = 13;
            // 
            // chbAnonymous
            // 
            this.chbAnonymous.AutoSize = true;
            this.chbAnonymous.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.chbAnonymous.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.chbAnonymous.Location = new System.Drawing.Point(419, 354);
            this.chbAnonymous.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbAnonymous.Name = "chbAnonymous";
            this.chbAnonymous.Size = new System.Drawing.Size(170, 23);
            this.chbAnonymous.TabIndex = 4;
            this.chbAnonymous.Text = "Send as anonymous";
            this.chbAnonymous.UseVisualStyleBackColor = true;
            // 
            // tbMessageBody
            // 
            this.tbMessageBody.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbMessageBody.Font = new System.Drawing.Font("Century Gothic", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMessageBody.Location = new System.Drawing.Point(35, 53);
            this.tbMessageBody.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbMessageBody.Multiline = true;
            this.tbMessageBody.Name = "tbMessageBody";
            this.tbMessageBody.Size = new System.Drawing.Size(691, 258);
            this.tbMessageBody.TabIndex = 3;
            // 
            // lbMessage
            // 
            this.lbMessage.AutoSize = true;
            this.lbMessage.Font = new System.Drawing.Font("Century Gothic", 7.8F);
            this.lbMessage.ForeColor = System.Drawing.Color.White;
            this.lbMessage.Location = new System.Drawing.Point(31, 20);
            this.lbMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(69, 19);
            this.lbMessage.TabIndex = 2;
            this.lbMessage.Text = "Message";
            // 
            // metrotpBalance
            // 
            this.metrotpBalance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpBalance.Controls.Add(this.rbtnPaymentMode);
            this.metrotpBalance.Controls.Add(this.rbtnPurchaseMode);
            this.metrotpBalance.Controls.Add(this.rbtnReportMode);
            this.metrotpBalance.Controls.Add(this.mtbPurchaseTotalPrice);
            this.metrotpBalance.Controls.Add(this.btnPurchaseAddItems);
            this.metrotpBalance.Controls.Add(this.lblPurchaseList);
            this.metrotpBalance.Controls.Add(this.lblPurchaseTotalPrice);
            this.metrotpBalance.Controls.Add(this.lbxBalance);
            this.metrotpBalance.Controls.Add(this.gbxPayment);
            this.metrotpBalance.Controls.Add(this.gbxPurchase);
            this.metrotpBalance.Controls.Add(this.gbxReport);
            this.metrotpBalance.HorizontalScrollbarBarColor = true;
            this.metrotpBalance.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpBalance.HorizontalScrollbarSize = 12;
            this.metrotpBalance.Location = new System.Drawing.Point(4, 38);
            this.metrotpBalance.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotpBalance.Name = "metrotpBalance";
            this.metrotpBalance.Size = new System.Drawing.Size(797, 695);
            this.metrotpBalance.TabIndex = 3;
            this.metrotpBalance.Text = "Balance";
            this.metrotpBalance.UseCustomBackColor = true;
            this.metrotpBalance.VerticalScrollbarBarColor = true;
            this.metrotpBalance.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpBalance.VerticalScrollbarSize = 13;
            // 
            // rbtnPaymentMode
            // 
            this.rbtnPaymentMode.AutoSize = true;
            this.rbtnPaymentMode.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.rbtnPaymentMode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rbtnPaymentMode.Location = new System.Drawing.Point(11, 447);
            this.rbtnPaymentMode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnPaymentMode.Name = "rbtnPaymentMode";
            this.rbtnPaymentMode.Size = new System.Drawing.Size(143, 24);
            this.rbtnPaymentMode.TabIndex = 9;
            this.rbtnPaymentMode.TabStop = true;
            this.rbtnPaymentMode.Text = "Payment Mode";
            this.rbtnPaymentMode.UseVisualStyleBackColor = true;
            this.rbtnPaymentMode.CheckedChanged += new System.EventHandler(this.ChangeMode_CheckedChanged);
            // 
            // rbtnPurchaseMode
            // 
            this.rbtnPurchaseMode.AutoSize = true;
            this.rbtnPurchaseMode.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.rbtnPurchaseMode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rbtnPurchaseMode.Location = new System.Drawing.Point(11, 144);
            this.rbtnPurchaseMode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnPurchaseMode.Name = "rbtnPurchaseMode";
            this.rbtnPurchaseMode.Size = new System.Drawing.Size(147, 24);
            this.rbtnPurchaseMode.TabIndex = 9;
            this.rbtnPurchaseMode.TabStop = true;
            this.rbtnPurchaseMode.Text = "Purchase Mode";
            this.rbtnPurchaseMode.UseVisualStyleBackColor = true;
            this.rbtnPurchaseMode.CheckedChanged += new System.EventHandler(this.ChangeMode_CheckedChanged);
            // 
            // rbtnReportMode
            // 
            this.rbtnReportMode.AutoSize = true;
            this.rbtnReportMode.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.rbtnReportMode.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.rbtnReportMode.Location = new System.Drawing.Point(11, 22);
            this.rbtnReportMode.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbtnReportMode.Name = "rbtnReportMode";
            this.rbtnReportMode.Size = new System.Drawing.Size(128, 24);
            this.rbtnReportMode.TabIndex = 9;
            this.rbtnReportMode.TabStop = true;
            this.rbtnReportMode.Text = "Report Mode";
            this.rbtnReportMode.UseVisualStyleBackColor = true;
            this.rbtnReportMode.CheckedChanged += new System.EventHandler(this.ChangeMode_CheckedChanged);
            // 
            // mtbPurchaseTotalPrice
            // 
            this.mtbPurchaseTotalPrice.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.mtbPurchaseTotalPrice.Location = new System.Drawing.Point(576, 564);
            this.mtbPurchaseTotalPrice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbPurchaseTotalPrice.Name = "mtbPurchaseTotalPrice";
            this.mtbPurchaseTotalPrice.Size = new System.Drawing.Size(187, 28);
            this.mtbPurchaseTotalPrice.TabIndex = 8;
            // 
            // btnPurchaseAddItems
            // 
            this.btnPurchaseAddItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnPurchaseAddItems.FlatAppearance.BorderSize = 0;
            this.btnPurchaseAddItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchaseAddItems.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnPurchaseAddItems.ForeColor = System.Drawing.SystemColors.Window;
            this.btnPurchaseAddItems.Location = new System.Drawing.Point(647, 615);
            this.btnPurchaseAddItems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPurchaseAddItems.Name = "btnPurchaseAddItems";
            this.btnPurchaseAddItems.Size = new System.Drawing.Size(117, 33);
            this.btnPurchaseAddItems.TabIndex = 3;
            this.btnPurchaseAddItems.Text = "Add Items";
            this.btnPurchaseAddItems.UseVisualStyleBackColor = false;
            this.btnPurchaseAddItems.Visible = false;
            this.btnPurchaseAddItems.Click += new System.EventHandler(this.btnPurchaseAddItems_Click);
            // 
            // lblPurchaseList
            // 
            this.lblPurchaseList.AutoSize = true;
            this.lblPurchaseList.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.lblPurchaseList.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPurchaseList.Location = new System.Drawing.Point(479, 62);
            this.lblPurchaseList.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPurchaseList.Name = "lblPurchaseList";
            this.lblPurchaseList.Size = new System.Drawing.Size(175, 20);
            this.lblPurchaseList.TabIndex = 8;
            this.lblPurchaseList.Text = "Purchase List (quantity)";
            this.lblPurchaseList.Click += new System.EventHandler(this.lblPurchaseList_Click);
            // 
            // lblPurchaseTotalPrice
            // 
            this.lblPurchaseTotalPrice.AutoSize = true;
            this.lblPurchaseTotalPrice.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.lblPurchaseTotalPrice.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPurchaseTotalPrice.Location = new System.Drawing.Point(475, 564);
            this.lblPurchaseTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPurchaseTotalPrice.Name = "lblPurchaseTotalPrice";
            this.lblPurchaseTotalPrice.Size = new System.Drawing.Size(83, 20);
            this.lblPurchaseTotalPrice.TabIndex = 8;
            this.lblPurchaseTotalPrice.Text = "Total Price";
            // 
            // lbxBalance
            // 
            this.lbxBalance.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.lbxBalance.FormattingEnabled = true;
            this.lbxBalance.ItemHeight = 20;
            this.lbxBalance.Location = new System.Drawing.Point(479, 100);
            this.lbxBalance.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbxBalance.Name = "lbxBalance";
            this.lbxBalance.Size = new System.Drawing.Size(284, 404);
            this.lbxBalance.TabIndex = 6;
            this.lbxBalance.SelectedIndexChanged += new System.EventHandler(this.lbxBalance_SelectedIndexChanged);
            // 
            // gbxPayment
            // 
            this.gbxPayment.Controls.Add(this.mtbPaymentAmount);
            this.gbxPayment.Controls.Add(this.btnPaymentUpdateBalane);
            this.gbxPayment.Controls.Add(this.label15);
            this.gbxPayment.Controls.Add(this.cbxPaymentResident);
            this.gbxPayment.Controls.Add(this.lbPaymentResident);
            this.gbxPayment.Enabled = false;
            this.gbxPayment.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPayment.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxPayment.Location = new System.Drawing.Point(11, 480);
            this.gbxPayment.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPayment.Name = "gbxPayment";
            this.gbxPayment.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPayment.Size = new System.Drawing.Size(445, 191);
            this.gbxPayment.TabIndex = 5;
            this.gbxPayment.TabStop = false;
            this.gbxPayment.Text = "Payment";
            // 
            // mtbPaymentAmount
            // 
            this.mtbPaymentAmount.Location = new System.Drawing.Point(95, 86);
            this.mtbPaymentAmount.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.mtbPaymentAmount.Name = "mtbPaymentAmount";
            this.mtbPaymentAmount.Size = new System.Drawing.Size(196, 26);
            this.mtbPaymentAmount.TabIndex = 8;
            // 
            // btnPaymentUpdateBalane
            // 
            this.btnPaymentUpdateBalane.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnPaymentUpdateBalane.FlatAppearance.BorderSize = 0;
            this.btnPaymentUpdateBalane.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPaymentUpdateBalane.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnPaymentUpdateBalane.ForeColor = System.Drawing.SystemColors.Window;
            this.btnPaymentUpdateBalane.Location = new System.Drawing.Point(272, 135);
            this.btnPaymentUpdateBalane.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPaymentUpdateBalane.Name = "btnPaymentUpdateBalane";
            this.btnPaymentUpdateBalane.Size = new System.Drawing.Size(167, 46);
            this.btnPaymentUpdateBalane.TabIndex = 3;
            this.btnPaymentUpdateBalane.Text = "Increase Balance";
            this.btnPaymentUpdateBalane.UseVisualStyleBackColor = false;
            this.btnPaymentUpdateBalane.Click += new System.EventHandler(this.btnPaymentUpdateBalane_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 92);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 20);
            this.label15.TabIndex = 6;
            this.label15.Text = "Amount";
            // 
            // cbxPaymentResident
            // 
            this.cbxPaymentResident.FormattingEnabled = true;
            this.cbxPaymentResident.Location = new System.Drawing.Point(95, 31);
            this.cbxPaymentResident.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxPaymentResident.Name = "cbxPaymentResident";
            this.cbxPaymentResident.Size = new System.Drawing.Size(196, 28);
            this.cbxPaymentResident.TabIndex = 0;
            this.cbxPaymentResident.SelectedIndexChanged += new System.EventHandler(this.cbxPaymentResident_SelectedIndexChanged);
            // 
            // lbPaymentResident
            // 
            this.lbPaymentResident.AutoSize = true;
            this.lbPaymentResident.Location = new System.Drawing.Point(8, 34);
            this.lbPaymentResident.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPaymentResident.Name = "lbPaymentResident";
            this.lbPaymentResident.Size = new System.Drawing.Size(71, 20);
            this.lbPaymentResident.TabIndex = 3;
            this.lbPaymentResident.Text = "Resident";
            // 
            // gbxPurchase
            // 
            this.gbxPurchase.Controls.Add(this.textBox2);
            this.gbxPurchase.Controls.Add(this.tbPurchaseQuantity);
            this.gbxPurchase.Controls.Add(this.tbPurchaseItem);
            this.gbxPurchase.Controls.Add(this.btnPurchaseRemoveItem);
            this.gbxPurchase.Controls.Add(this.btnPurchaseAddItem);
            this.gbxPurchase.Controls.Add(this.cbxPurchaseResident);
            this.gbxPurchase.Controls.Add(this.dtpPurchesDate);
            this.gbxPurchase.Controls.Add(this.lbPurchaseQuantity);
            this.gbxPurchase.Controls.Add(this.lbPurchaseItem);
            this.gbxPurchase.Controls.Add(this.lbPurchaseResident);
            this.gbxPurchase.Controls.Add(this.lbPurchaseDate);
            this.gbxPurchase.Enabled = false;
            this.gbxPurchase.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxPurchase.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbxPurchase.Location = new System.Drawing.Point(11, 177);
            this.gbxPurchase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPurchase.Name = "gbxPurchase";
            this.gbxPurchase.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbxPurchase.Size = new System.Drawing.Size(445, 262);
            this.gbxPurchase.TabIndex = 5;
            this.gbxPurchase.TabStop = false;
            this.gbxPurchase.Text = "Purchase";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(-307, -126);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(132, 26);
            this.textBox2.TabIndex = 10;
            // 
            // tbPurchaseQuantity
            // 
            this.tbPurchaseQuantity.Location = new System.Drawing.Point(95, 167);
            this.tbPurchaseQuantity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbPurchaseQuantity.Name = "tbPurchaseQuantity";
            this.tbPurchaseQuantity.Size = new System.Drawing.Size(196, 26);
            this.tbPurchaseQuantity.TabIndex = 10;
            // 
            // tbPurchaseItem
            // 
            this.tbPurchaseItem.Location = new System.Drawing.Point(95, 123);
            this.tbPurchaseItem.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbPurchaseItem.Name = "tbPurchaseItem";
            this.tbPurchaseItem.Size = new System.Drawing.Size(196, 26);
            this.tbPurchaseItem.TabIndex = 10;
            // 
            // btnPurchaseRemoveItem
            // 
            this.btnPurchaseRemoveItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnPurchaseRemoveItem.FlatAppearance.BorderSize = 0;
            this.btnPurchaseRemoveItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchaseRemoveItem.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnPurchaseRemoveItem.ForeColor = System.Drawing.SystemColors.Window;
            this.btnPurchaseRemoveItem.Location = new System.Drawing.Point(152, 210);
            this.btnPurchaseRemoveItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPurchaseRemoveItem.Name = "btnPurchaseRemoveItem";
            this.btnPurchaseRemoveItem.Size = new System.Drawing.Size(140, 41);
            this.btnPurchaseRemoveItem.TabIndex = 3;
            this.btnPurchaseRemoveItem.Text = "Remove Item";
            this.btnPurchaseRemoveItem.UseVisualStyleBackColor = false;
            this.btnPurchaseRemoveItem.Click += new System.EventHandler(this.btnPurchaseRemoveItem_Click);
            // 
            // btnPurchaseAddItem
            // 
            this.btnPurchaseAddItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(173)))), ((int)(((byte)(135)))));
            this.btnPurchaseAddItem.FlatAppearance.BorderSize = 0;
            this.btnPurchaseAddItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPurchaseAddItem.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.btnPurchaseAddItem.ForeColor = System.Drawing.SystemColors.Window;
            this.btnPurchaseAddItem.Location = new System.Drawing.Point(299, 210);
            this.btnPurchaseAddItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPurchaseAddItem.Name = "btnPurchaseAddItem";
            this.btnPurchaseAddItem.Size = new System.Drawing.Size(140, 41);
            this.btnPurchaseAddItem.TabIndex = 3;
            this.btnPurchaseAddItem.Text = "Add Item";
            this.btnPurchaseAddItem.UseVisualStyleBackColor = false;
            this.btnPurchaseAddItem.Click += new System.EventHandler(this.btnPurchaseAddItem_Click);
            // 
            // cbxPurchaseResident
            // 
            this.cbxPurchaseResident.FormattingEnabled = true;
            this.cbxPurchaseResident.Location = new System.Drawing.Point(95, 73);
            this.cbxPurchaseResident.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxPurchaseResident.Name = "cbxPurchaseResident";
            this.cbxPurchaseResident.Size = new System.Drawing.Size(196, 28);
            this.cbxPurchaseResident.TabIndex = 0;
            this.cbxPurchaseResident.SelectedIndexChanged += new System.EventHandler(this.cbxPurchaseResident_SelectedIndexChanged);
            // 
            // dtpPurchesDate
            // 
            this.dtpPurchesDate.CustomFormat = "yyyy-MM-dd";
            this.dtpPurchesDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPurchesDate.Location = new System.Drawing.Point(95, 28);
            this.dtpPurchesDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpPurchesDate.Name = "dtpPurchesDate";
            this.dtpPurchesDate.Size = new System.Drawing.Size(196, 26);
            this.dtpPurchesDate.TabIndex = 9;
            // 
            // lbPurchaseQuantity
            // 
            this.lbPurchaseQuantity.AutoSize = true;
            this.lbPurchaseQuantity.Location = new System.Drawing.Point(12, 171);
            this.lbPurchaseQuantity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPurchaseQuantity.Name = "lbPurchaseQuantity";
            this.lbPurchaseQuantity.Size = new System.Drawing.Size(71, 20);
            this.lbPurchaseQuantity.TabIndex = 8;
            this.lbPurchaseQuantity.Text = "Quantity";
            // 
            // lbPurchaseItem
            // 
            this.lbPurchaseItem.AutoSize = true;
            this.lbPurchaseItem.Location = new System.Drawing.Point(12, 123);
            this.lbPurchaseItem.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPurchaseItem.Name = "lbPurchaseItem";
            this.lbPurchaseItem.Size = new System.Drawing.Size(42, 20);
            this.lbPurchaseItem.TabIndex = 8;
            this.lbPurchaseItem.Text = "Item";
            // 
            // lbPurchaseResident
            // 
            this.lbPurchaseResident.AutoSize = true;
            this.lbPurchaseResident.Location = new System.Drawing.Point(8, 76);
            this.lbPurchaseResident.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPurchaseResident.Name = "lbPurchaseResident";
            this.lbPurchaseResident.Size = new System.Drawing.Size(71, 20);
            this.lbPurchaseResident.TabIndex = 5;
            this.lbPurchaseResident.Text = "Resident";
            // 
            // lbPurchaseDate
            // 
            this.lbPurchaseDate.AutoSize = true;
            this.lbPurchaseDate.Location = new System.Drawing.Point(8, 34);
            this.lbPurchaseDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPurchaseDate.Name = "lbPurchaseDate";
            this.lbPurchaseDate.Size = new System.Drawing.Size(45, 20);
            this.lbPurchaseDate.TabIndex = 3;
            this.lbPurchaseDate.Text = "Date";
            // 
            // metrotpRules
            // 
            this.metrotpRules.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(39)))), ((int)(((byte)(40)))));
            this.metrotpRules.Controls.Add(this.tbRules);
            this.metrotpRules.HorizontalScrollbarBarColor = true;
            this.metrotpRules.HorizontalScrollbarHighlightOnWheel = false;
            this.metrotpRules.HorizontalScrollbarSize = 12;
            this.metrotpRules.Location = new System.Drawing.Point(4, 38);
            this.metrotpRules.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.metrotpRules.Name = "metrotpRules";
            this.metrotpRules.Size = new System.Drawing.Size(797, 695);
            this.metrotpRules.TabIndex = 4;
            this.metrotpRules.Text = "Rules";
            this.metrotpRules.UseCustomBackColor = true;
            this.metrotpRules.VerticalScrollbarBarColor = true;
            this.metrotpRules.VerticalScrollbarHighlightOnWheel = false;
            this.metrotpRules.VerticalScrollbarSize = 13;
            // 
            // tbRules
            // 
            this.tbRules.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tbRules.Location = new System.Drawing.Point(4, 27);
            this.tbRules.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbRules.Multiline = true;
            this.tbRules.Name = "tbRules";
            this.tbRules.ReadOnly = true;
            this.tbRules.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRules.Size = new System.Drawing.Size(748, 614);
            this.tbRules.TabIndex = 2;
            // 
            // tmAnnouncements
            // 
            this.tmAnnouncements.Interval = 1000;
            this.tmAnnouncements.Tick += new System.EventHandler(this.tmAnnouncements_Tick);
            // 
            // ResidentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1192, 785);
            this.Controls.Add(this.metrotbcMain);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlSide);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "ResidentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ResidentForm";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form_MouseMove);
            this.pnlSide.ResumeLayout(false);
            this.gbxBlackList.ResumeLayout(false);
            this.gbxReport.ResumeLayout(false);
            this.gbxReport.PerformLayout();
            this.metrotbcMain.ResumeLayout(false);
            this.metrotpAgenda.ResumeLayout(false);
            this.metrotpAgenda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLeftArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRightArrow)).EndInit();
            this.metrotpCreateEvent.ResumeLayout(false);
            this.metrotpCreateEvent.PerformLayout();
            this.gbEnd.ResumeLayout(false);
            this.gbEnd.PerformLayout();
            this.gbStart.ResumeLayout(false);
            this.gbStart.PerformLayout();
            this.metrotpReport.ResumeLayout(false);
            this.metrotpReport.PerformLayout();
            this.metrotpBalance.ResumeLayout(false);
            this.metrotpBalance.PerformLayout();
            this.gbxPayment.ResumeLayout(false);
            this.gbxPayment.PerformLayout();
            this.gbxPurchase.ResumeLayout(false);
            this.gbxPurchase.PerformLayout();
            this.metrotpRules.ResumeLayout(false);
            this.metrotpRules.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSide;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlAnnouncement;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSendMessage;
        private MetroFramework.Controls.MetroTabControl metrotbcMain;
        private MetroFramework.Controls.MetroTabPage metrotpAgenda;
        private MetroFramework.Controls.MetroTabPage metrotpCreateEvent;
        private MetroFramework.Controls.MetroTabPage metrotpReport;
        private MetroFramework.Controls.MetroTabPage metrotpBalance;
        private MetroFramework.Controls.MetroTabPage metrotpRules;
        private System.Windows.Forms.TextBox tbRules;
        private System.Windows.Forms.Label lbMessage;
        private System.Windows.Forms.TextBox tbMessageBody;
        private System.Windows.Forms.CheckBox chbAnonymous;
        private System.Windows.Forms.ComboBox cbTypeofevent;
        private System.Windows.Forms.Label lbTypeofevent;
        private System.Windows.Forms.Label lbHost;
        private System.Windows.Forms.Label lbHostname;
        private System.Windows.Forms.GroupBox gbStart;
        private System.Windows.Forms.GroupBox gbEnd;
        private System.Windows.Forms.Label lbEndTime;
        private System.Windows.Forms.Label lbEndDate;
        private System.Windows.Forms.Label lbStartTime;
        private System.Windows.Forms.Label lbStartDate;
        private System.Windows.Forms.Button btnCreateEvent;
        private System.Windows.Forms.Button btnAboutUs;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.Panel pnlAgenda;
        private System.Windows.Forms.Button btnGenerateChores;
        private System.Windows.Forms.Label lblWeek;
        private System.Windows.Forms.PictureBox pbxRightArrow;
        private System.Windows.Forms.PictureBox pbxLeftArrow;
        private System.Windows.Forms.GroupBox gbxReport;
        private System.Windows.Forms.ListBox lbxBalance;
        private System.Windows.Forms.GroupBox gbxPayment;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lbPaymentResident;
        private System.Windows.Forms.GroupBox gbxPurchase;
        private System.Windows.Forms.Label lbPurchaseResident;
        private System.Windows.Forms.Label lblMonth;
        private System.Windows.Forms.Label lbPurchaseDate;
        private System.Windows.Forms.Button btnPurchaseAddItems;
        private System.Windows.Forms.ComboBox cbxReportMonth;
        private System.Windows.Forms.MaskedTextBox mtbPurchaseTotalPrice;
        private System.Windows.Forms.DateTimePicker dtpPurchesDate;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnPurchaseAddItem;
        private System.Windows.Forms.ComboBox cbxPaymentResident;
        private System.Windows.Forms.MaskedTextBox mtbPaymentAmount;
        private System.Windows.Forms.Button btnPaymentUpdateBalane;
        private System.Windows.Forms.ComboBox cbxPurchaseResident;
        private System.Windows.Forms.TextBox tbPurchaseItem;
        private System.Windows.Forms.Label lbPurchaseItem;
        private System.Windows.Forms.RadioButton rbtnReportMode;
        private System.Windows.Forms.RadioButton rbtnPaymentMode;
        private System.Windows.Forms.RadioButton rbtnPurchaseMode;
        private System.Windows.Forms.TextBox tbPurchaseQuantity;
        private System.Windows.Forms.Label lbPurchaseQuantity;
        private System.Windows.Forms.GroupBox gbxBlackList;
        private System.Windows.Forms.ListBox lbxBlackList;
        private System.Windows.Forms.Button btnPurchaseRemoveItem;
        private System.Windows.Forms.Label lblPurchaseTotalPrice;
        private System.Windows.Forms.Timer tmAnnouncements;
        private System.Windows.Forms.Label lblPurchaseList;
    }
}

