﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentHouse
{
    public partial class ResidentForm : Form
    {
        int user_id;
        bool IsRep;
        DateTime sunday;
        DateTime saturday;
        System.Collections.Hashtable curUser_ResidentInfo;
        DataTable dt_purchases;
        DataTable dt_newAnnouncements;
        DataTable dt_curAnnouncements;

        public ResidentForm()
        {
            InitializeComponent();
        }

        public ResidentForm(User currentUser)
        {
            InitializeComponent();
            curUser_ResidentInfo = currentUser.GetResidentInfo();
            IsRep = currentUser.IsRep;
            user_id = Convert.ToInt32(curUser_ResidentInfo["ID"]);
            lbHostname.Text = curUser_ResidentInfo["FirstName"].ToString() + " " + curUser_ResidentInfo["LastName"].ToString();
            dtpStartDate.Text = DateTime.Now.ToShortDateString();
            dtpEndDate.Text = DateTime.Now.ToShortDateString();
            dtpStartTime.Text = DateTime.Now.ToString("HH:mm");
            dtpEndTime.Text = DateTime.Now.ToString("HH:mm");
            sunday = DateTime.Today.AddDays(-(double)DateTime.Today.DayOfWeek);
            saturday = DateTime.Today.AddDays(-(double)DateTime.Today.DayOfWeek + (double)DayOfWeek.Saturday);
            metrotbcMain.SelectedTab = metrotpAgenda;
            metrotbcMain_SelectedIndexChanged(null, null);

            dt_purchases = new DataTable();
            dt_purchases.Columns.Add("Date", typeof(string));
            dt_purchases.Columns.Add("resident_id", typeof(string));
            dt_purchases.Columns.Add("item", typeof(string));
            dt_purchases.Columns.Add("quantity", typeof(string));
            
            dt_newAnnouncements = new DataTable();
            dt_curAnnouncements = new DataTable();
            tmAnnouncements.Start();
            UpdateBlackList();

        }
        
        private void btnCreateEvent_Click(object sender, EventArgs e)
        {
            byte type = Convert.ToByte((int)cbTypeofevent.SelectedIndex + 1);
            Event newEvent = new Event(type, user_id);

            string start_date = dtpStartDate.Value.ToString("yyyy-MM-dd");
            string start_time = dtpStartTime.Value.ToString("HH:mm:ss");
            string end_date = dtpEndDate.Value.ToString("yyyy-MM-dd");
            string end_time = dtpEndTime.Value.ToString("HH:mm:ss");

            newEvent.SetDateTime(start_date, start_time, end_date, end_time);
            string errmsg = newEvent.InsertEvent();

            if (string.IsNullOrEmpty(errmsg))
            {
                string ptitle = cbTypeofevent.SelectedItem.ToString();
                string pbody = start_date + "|" + start_time + " until " + end_date + "|" + end_time;
                pbody += "\n" + "Host : " + lbHostname.Text;
                Announcement.SendAnnouncementToAllUsers(user_id, ptitle, pbody);
                MessageBox.Show("An event has been created!");
            }
            else
                MessageBox.Show(errmsg);
        }


        private void btnSendMessage_Click(object sender, EventArgs e)
        {
            Message message;
            string errmsg;
            if (tbMessageBody.Text == "")
            {
                MessageBox.Show("Please insert a message.");
                return;
            }
            if (chbAnonymous.Checked)
            {
                message = new Message(tbMessageBody.Text);
                errmsg = message.InsertMessage();
                if (string.IsNullOrEmpty(errmsg))
                {
                    MessageBox.Show("Message successfully sent!");
                    tbMessageBody.Clear();
                    return;
                }
                MessageBox.Show(errmsg);
                return;
            }
            message = new Message(user_id, tbMessageBody.Text);
            errmsg = message.InsertMessage();
            if (string.IsNullOrEmpty(errmsg))
            {
                MessageBox.Show("Message successfully sent!");
                tbMessageBody.Clear();
                return;
            }
            MessageBox.Show(errmsg);
            return;
        }

        private void btnAboutUs_Click(object sender, EventArgs e)
        {
            MessageBox.Show("WE ARE FOURSTANDING!", "About Us");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            LoginForm login = new LoginForm();
            login.Show();
        }

        private void metrotbcMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (metrotbcMain.SelectedTab == metrotpRules)
            {
                Database database = new Database();
                tbRules.Text = database.GetData("SELECT body from rules").Rows[0][0].ToString();
                return;
            }
            if (metrotbcMain.SelectedTab == metrotpAgenda)
            {
                btnGenerateChores.Visible = IsRep;
                pnlAgenda.Controls.Clear();
                pbxLeftArrow.Visible = sunday.CompareTo(new DateTime(DateTime.Today.Year, 1, 1)) == -1 ? false : true;
                pbxRightArrow.Visible = saturday.CompareTo(new DateTime(DateTime.Today.Year, 12, 31)) == 1 ? false : true;
                lblWeek.Text = sunday.ToString("yyyy-MM-dd") + " Until " + saturday.ToString("yyyy-MM-dd");
                int heightofpanels = 0;
                DataTable weekagenda_dt = Event.GetEvents(sunday.ToString("yyyy-MM-dd"), saturday.ToString("yyyy-MM-dd"));
                foreach (DataRow row in weekagenda_dt.Rows)
                {
                    Panel pnlevent = new Panel();
                    pnlevent.Name = "pnlevent_" + row["ID"].ToString();
                    Label lblevent_datetime = new Label();
                    Label lblevent_typeofevent = new Label();

                    pnlevent.Parent = pnlAgenda;
                    pnlevent.Width = pnlAgenda.Size.Width - 30;
                    pnlevent.Height = 110;
                    pnlevent.BackColor = Color.FromArgb(7, 173, 135);
                    pnlevent.Left = 7;
                    pnlevent.Top = heightofpanels + 10;
                    heightofpanels += pnlevent.Height + 10;

                    if (Convert.ToByte(row["typeof_event"]) == 6 || Convert.ToByte(row["typeof_event"]) == 7)
                    {
                        Label lblevent_responsibleRoom = new Label();
                        lblevent_responsibleRoom.AutoSize = true;
                        lblevent_responsibleRoom.Parent = pnlevent;
                        lblevent_responsibleRoom.Text = "Responsible Room: " + row["responsible_room"].ToString();
                        lblevent_responsibleRoom.Top = 70;
                        lblevent_responsibleRoom.Left = 10;
                        lblevent_responsibleRoom.Font = new Font("Century Gothic", 9, FontStyle.Regular);
                        lblevent_responsibleRoom.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                        lblevent_responsibleRoom.BackColor = Color.FromKnownColor(KnownColor.Transparent);
                    }
                    else
                    {
                        Label lblevent_host = new Label();
                        lblevent_host.AutoSize = true;
                        System.Collections.Hashtable hostInfo = User.GetResidentInfoByUserId(Convert.ToInt32(row["created_by"]));
                        lblevent_host.Parent = pnlevent;
                        if (Convert.ToByte(row["typeof_event"]) != 5)
                            lblevent_host.Text = "Host: " + hostInfo["FirstName"].ToString() + " " + hostInfo["LastName"].ToString();
                        else
                            lblevent_host.Text = "Host: Agent";
                        lblevent_host.Top = 70;
                        lblevent_host.Left = 10;
                        lblevent_host.Font = new Font("Century Gothic", 9, FontStyle.Regular);
                        lblevent_host.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                        lblevent_host.BackColor = Color.FromKnownColor(KnownColor.Transparent);
                    }

                    lblevent_datetime.Parent = pnlevent;
                    lblevent_datetime.AutoSize = true;
                    lblevent_datetime.Text = ((DateTime)row["StartDate"]).ToString("yyyy-MM-dd")
                                           + "|" + row["StartTime"].ToString()
                                           + " Until " + ((DateTime)row["EndDate"]).ToString("yyyy-MM-dd")
                                           + "|" + row["EndTime"].ToString();
                    lblevent_datetime.Top = 50;
                    lblevent_datetime.Left = 10;
                    lblevent_datetime.Font = new Font("Century Gothic", 9, FontStyle.Regular);
                    lblevent_datetime.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    lblevent_datetime.BackColor = Color.FromKnownColor(KnownColor.Transparent);

                    lblevent_typeofevent.Parent = pnlevent;
                    lblevent_typeofevent.AutoSize = true;
                    switch (Convert.ToByte(row["typeof_event"]))
                    {
                        case 1:
                            lblevent_typeofevent.Text = "Birthday Party";
                            break;
                        case 2:
                            lblevent_typeofevent.Text = "Dinner Party";
                            break;
                        case 3:
                            lblevent_typeofevent.Text = "Small Gathering";
                            break;
                        case 4:
                            lblevent_typeofevent.Text = "Game Night Party";
                            break;
                        case 5:
                            lblevent_typeofevent.Text = "Events from Agent";
                            break;
                        case 6:
                            lblevent_typeofevent.Text = "Cleaning";
                            break;
                        case 7:
                            lblevent_typeofevent.Text = "Shopping";
                            break;
                        default:
                            break;
                    }
                    lblevent_typeofevent.Top = 10;
                    lblevent_typeofevent.Left = 10;
                    lblevent_typeofevent.Font = new Font("Century Gothic", 11, FontStyle.Bold);
                    lblevent_typeofevent.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    lblevent_typeofevent.BackColor = Color.FromKnownColor(KnownColor.Transparent);
                }
            }

            if (metrotbcMain.SelectedTab == metrotpBalance)
            {
                rbtnPurchaseMode.Visible = IsRep;
                rbtnPaymentMode.Visible = IsRep;
                gbxPurchase.Visible = IsRep;
                gbxPayment.Visible = IsRep;
                btnPurchaseAddItems.Visible = IsRep;
                mtbPurchaseTotalPrice.ReadOnly = !IsRep;
                rbtnReportMode.Checked = true;
                ChangeMode_CheckedChanged(null, null);

                lbxBalance.Items.Clear();
            }

        }

        private void btnarch_Click(object sender, EventArgs e)
        {
            Button btnarch = sender as Button;
            int reduced_top = btnarch.Parent.Top;
            Announcement.Archive(btnarch.Name.Split('_')[1]);
            pnlAnnouncement.Controls.Remove(btnarch.Parent);
            foreach (Control ctl in pnlAnnouncement.Controls)
            {
                if (ctl.Name.StartsWith("pnlannouncement"))
                {
                    ctl.Top -= reduced_top;
                }
            }
        }

        private void btnmore_Click(object sender, EventArgs e)
        {
            Button btnmore = sender as Button;
            ReadMore rm = new ReadMore();
            rm.Show();
        }

        private void btnGenerateChores_Click(object sender, EventArgs e)
        {
            pnlAgenda.Controls.Clear();
            Event.GnerateChores(DateTime.Today, new DateTime(DateTime.Today.Year, 12, 31));
            metrotbcMain_SelectedIndexChanged(null, null);
        }

        private void pbxRightArrow_Click(object sender, EventArgs e)
        {
            sunday = sunday.AddDays(7);
            saturday = saturday.AddDays(7);

            metrotbcMain_SelectedIndexChanged(null, null);

        }

        private void pbxLeftArrow_Click(object sender, EventArgs e)
        {
            sunday = sunday.AddDays(-7);
            saturday = saturday.AddDays(-7);
            metrotbcMain_SelectedIndexChanged(null, null);
        }

        #region MouseMethods

        Point lastClick; //Holds where the Form was clicked
        private void Form_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }
        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        #endregion MouseMethods
        private void pnlAnnouncement_Paint(object sender, PaintEventArgs e)
        {

        }

        #region BalanceTab

        private void ChangeMode_CheckedChanged(object sender, EventArgs e)
        {
            gbxReport.Enabled = rbtnReportMode.Checked;
            gbxPurchase.Enabled = rbtnPurchaseMode.Checked;
            gbxPayment.Enabled = rbtnPaymentMode.Checked;
            lbxBalance.Items.Clear();
            if (gbxReport.Enabled)
            {
                string[] items = { "January", "February", "March", "April", "May"
                                , "June","July","August","September", "October", "November", "December" };
                cbxReportMonth.Items.Clear();
                cbxReportMonth.Items.AddRange(items);
                lblPurchaseList.Text = "Purchase List (quantity)";
            }
            if (gbxPurchase.Enabled)
            {
                DataTable data = Resident.GetList_Residents();
                data.Columns.Add("FullName", typeof(string), "FirstName + '' + LastName");
                cbxPurchaseResident.DataSource = data;

                cbxPurchaseResident.DisplayMember = "FullName";
                cbxPurchaseResident.ValueMember = "ID";
                lblPurchaseList.Text = "Purchase List (quantity)";
            }
            if (gbxPayment.Enabled)
            {
                DataTable data = Resident.GetList_Residents();
                data.Columns.Add("FullName", typeof(string), "FirstName + '' + LastName");
                cbxPaymentResident.DisplayMember = "FullName";
                cbxPaymentResident.ValueMember = "ID";
                cbxPaymentResident.DataSource = data;
                lblPurchaseList.Text = "Months Paid List";
            }
        }

        private void cbxReportMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxReportMonth.SelectedIndex != -1)
            {
                int month = cbxReportMonth.SelectedIndex + 1;
                DataTable dt_Report = Purchase.GetList_Purchases("MONTH(purchase_date) = " + month.ToString());
                lbxBalance.Items.Clear();
                if (dt_Report.Rows.Count == 0)
                {
                    lbxBalance.Items.Add("There is no item to show");
                    return;
                }
                foreach (DataRow row in dt_Report.Rows)
                {
                    string[] items = row["purchased_items"].ToString().Split('#');
                    lbxBalance.Items.AddRange(items);
                }

                mtbPurchaseTotalPrice.Text = dt_Report.Compute("SUM(totalprice)", string.Empty).ToString();
            }
        }

        private void btnPurchaseAddItem_Click(object sender, EventArgs e)
        {
            DataRow row = dt_purchases.NewRow();
            if (string.IsNullOrEmpty(dtpPurchesDate.Value.ToString()))
            {
                MessageBox.Show("Please enter a date");
                return;
            }
            if (string.IsNullOrEmpty(tbPurchaseItem.Text))
            {
                MessageBox.Show("Please enter name of item");
                return;
            }
            if (string.IsNullOrEmpty(tbPurchaseQuantity.Text))
            {
                MessageBox.Show("Please enter quantity of item");
                return;
            }

            row["Date"] = dtpPurchesDate.Value.ToString("yyyy-MM-dd");
            row["resident_id"] = cbxPurchaseResident.SelectedValue.ToString();
            row["item"] = tbPurchaseItem.Text;
            row["quantity"] = tbPurchaseQuantity.Text;
            if (dt_purchases.Rows.Count > 0 &&
                dt_purchases.Rows[0]["Date"].ToString() != dtpPurchesDate.Value.ToString("yyyy-MM-dd"))
            {
                MessageBox.Show("Date field for all items should be " + dt_purchases.Rows[0]["Date"].ToString());
                return;
            }
            if (dt_purchases.Rows.Count > 0 &&
                dt_purchases.Rows[0]["resident_id"].ToString() != cbxPurchaseResident.SelectedValue.ToString())
            {
                MessageBox.Show("Resident field for all items should be the same.");
                return;
            }
            dt_purchases.Rows.Add(row);
            lbxBalance.Items.Add(row["item"].ToString() + "(" + row["quantity"].ToString() + ")");
            tbPurchaseItem.Clear();
            tbPurchaseQuantity.Clear();
        }

        private void btnPaymentUpdateBalane_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(mtbPaymentAmount.Text))
            {
                MessageBox.Show("Please enter an amount to update the balance");
                return;
            }
            DataRow cur_row = Resident.GetList_Residents("ID = " + cbxPaymentResident.SelectedValue.ToString()).Rows[0];
            Resident selected_resident = new Resident(cur_row);
            selected_resident.IncreaseBalance(Convert.ToDouble(mtbPaymentAmount.Text));
            mtbPaymentAmount.Clear();
            cbxPaymentResident_SelectedIndexChanged(null, null);
            UpdateBlackList();
        }

        private void btnPurchaseAddItems_Click(object sender, EventArgs e)
        {
            if (dt_purchases.Rows.Count == 0)
            {
                MessageBox.Show("There is no item in the list");
                return;
            }
            if (string.IsNullOrEmpty(mtbPurchaseTotalPrice.Text))
            {
                MessageBox.Show("Please enter the total price");
                return;
            }
            string purchase_date = dt_purchases.Rows[0]["Date"].ToString();
            int responsible_resident = Convert.ToInt32(dt_purchases.Rows[0]["resident_id"]);
            string purchased_items = string.Empty;
            foreach (DataRow row in dt_purchases.Rows)
            {
                purchased_items += row["item"].ToString() + "(" + row["quantity"].ToString() + ")#";
            }
            purchased_items = purchased_items.Remove(purchased_items.Length - 1);
            double price = Convert.ToDouble(mtbPurchaseTotalPrice.Text);
            Purchase new_purchase = new Purchase(responsible_resident, purchased_items, price, purchase_date);
            string errmsg = new_purchase.InsertPurchase();

            if (!string.IsNullOrEmpty(errmsg))
                MessageBox.Show(errmsg);
            else
            { 
                MessageBox.Show("Purchased Items added successfully");
                mtbPurchaseTotalPrice.Clear();
                lbxBalance.Items.Clear();
            }
        }

        private void cbxPaymentResident_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxPaymentResident.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a resident from the list");
                return;
            }
            lbxBalance.Items.Clear();
            DataRow cur_row = Resident.GetList_Residents("ID = " + cbxPaymentResident.SelectedValue.ToString()).Rows[0];
            Resident selected_resident = new Resident(cur_row);
            double balance = Convert.ToDouble(cur_row["Balance"]);
            int first_month = ((DateTime)cur_row["StartDate"]).Month;
            int first_year = ((DateTime)cur_row["StartDate"]).Year;

            string[] months = { "January", "February", "March", "April", "May"
                                , "June","July","August","September", "October", "November", "December" };

            double fee = 5.00;
            int months_count = Convert.ToInt32(balance / fee);
            if (months_count == 0)
            {
                MessageBox.Show("Resident's balance is not enough!");
                return;
            }

            while (months_count > 0)
            {
                lbxBalance.Items.Add(months[first_month - 1] + " " + first_year.ToString());
                first_month++;
                months_count--;
                if (first_month == 13)
                { 
                    first_month = 1;
                    first_year++;
                }
            }
        }

        private void cbxPurchaseResident_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lbxBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbtnPurchaseMode.Checked)
            {

            }
        }

        private void btnPurchaseRemoveItem_Click(object sender, EventArgs e)
        {
            if (lbxBalance.SelectedIndex != -1)
            {
                dt_purchases.Rows.RemoveAt(lbxBalance.SelectedIndex);
                lbxBalance.Items.RemoveAt(lbxBalance.SelectedIndex);
            }
        }
        private void UpdateBlackList()
        {
            List<string> blackList = Resident.GenerateBlackList();
            lbxBlackList.Items.Clear();
            foreach (string item in blackList)
            {
                lbxBlackList.Items.Add(item);
            }
        }

        #endregion BalanceTab

        private void tmAnnouncements_Tick(object sender, EventArgs e)
        {
            dt_newAnnouncements = Announcement.GetAllAnnouncements(user_id);
            if (dt_curAnnouncements.Rows.Count != dt_newAnnouncements.Rows.Count)
            { 
                dt_curAnnouncements = dt_newAnnouncements;
                pnlAnnouncement.Controls.Clear();
                int heightofpanels = 0;
                foreach (DataRow row in dt_curAnnouncements.Rows)
                {
                    Panel pnlannounce = new Panel();
                    pnlannounce.Name = "pnlannouncement" + row["ID"].ToString();
                    Button btnarch = new Button();
                    Label lblbody = new Label();
                    Label lbltitle = new Label();

                    pnlannounce.Parent = pnlAnnouncement;
                    pnlannounce.Width = pnlAnnouncement.Size.Width - 30;
                    pnlannounce.Height = 120;
                    pnlannounce.BackColor = Color.Transparent;
                    pnlannounce.Left = 7;
                    pnlannounce.Top = heightofpanels + 7;
                    pnlannounce.BorderStyle = BorderStyle.FixedSingle;
                    heightofpanels += pnlannounce.Height + 10;

                    btnarch.Parent = pnlannounce;
                    btnarch.Name = "btnannouncementArchive_" + row["ID"].ToString();
                    btnarch.Width = 60;
                    btnarch.Height = 25;
                    btnarch.Text = "Archive";
                    btnarch.Top = 90;
                    btnarch.Left = pnlannounce.Size.Width - (btnarch.Width + 7);
                    btnarch.Font = new Font("Century Gothic", 8, FontStyle.Regular);
                    btnarch.ForeColor = Color.FromKnownColor(KnownColor.Window);
                    btnarch.BackColor = Color.FromArgb(7, 173, 135);
                    btnarch.FlatStyle = FlatStyle.Flat;
                    btnarch.FlatAppearance.BorderSize = 0;
                    btnarch.Click += new System.EventHandler(btnarch_Click);

                    lblbody.Parent = pnlannounce;
                    lblbody.Height = 70;
                    lblbody.Width = 220;
                    lblbody.Text = row["body"].ToString();
                    lblbody.Top = 50;
                    lblbody.Left = 10;
                    lblbody.Font = new Font("Century Gothic", 9, FontStyle.Regular);
                    lblbody.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    lblbody.BackColor = Color.FromKnownColor(KnownColor.Transparent);

                    lbltitle.Parent = pnlannounce;
                    lbltitle.Height = 70;
                    lbltitle.Width = 220;
                    lbltitle.Text = row["title"].ToString();
                    lbltitle.Top = 10;
                    lbltitle.Left = 10;
                    lbltitle.Font = new Font("Century Gothic", 10, FontStyle.Bold);
                    lbltitle.ForeColor = Color.FromKnownColor(KnownColor.ButtonFace);
                    lbltitle.BackColor = Color.FromKnownColor(KnownColor.Transparent);
                }
            } 
        }

        private void lblPurchaseList_Click(object sender, EventArgs e)
        {

        }
    }
}
