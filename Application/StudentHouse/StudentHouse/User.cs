﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using System.Reflection;

namespace StudentHouse
{
    public class User
    {
        int ID;
        string username;
        string password;
        byte typeofuser;// type_of_user can be 1: resident, 2: rep, 3: agent 
        int Resident_id;
        private string created_at;
        private string updated_at;

        private static Database database = new Database();
        public bool IsRep { get { return this.typeofuser == 2 ? true : false; } }
        public User(Resident resident)
        {
            this.ID = database.GetLastID("Users",string.Empty) + 1;
            this.typeofuser = 1;
            this.Resident_id = Convert.ToInt32(resident.GetResidentInfo()["ID"]);
            this.AssignUsernamePassword(resident.GetResidentInfo()["FirstName"].ToString());
            this.created_at = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            this.updated_at = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            this.InsertUser();
        }
        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
            this.typeofuser = 1;
            this.Resident_id = -1;
            this.created_at = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            this.updated_at = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }
        public User(DataRow user_row)
        {
            this.ID = Convert.ToInt32(user_row["ID"]);
            this.username = user_row["username"].ToString();
            this.password = user_row["password"].ToString();
            this.typeofuser = Convert.ToByte(user_row["typeofuser"]);
            this.Resident_id = Convert.ToInt32(user_row["Resident_id"]);
            this.created_at = string.Empty;
        }

        public void InsertUser()
        {
            if (this.CheckInsertConditions() != string.Empty)
            {
                Console.WriteLine(this.CheckInsertConditions());
                return;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(User).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "created_at" || field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.InsertData(data, "Users");
        }
        public void SetAsRep()
        {
            this.typeofuser = 2;
            this.UpdateUser();
        }

        public void UpdateUser()
        {
            if (this.CheckUpdateConditions() != string.Empty)
            {
                Console.WriteLine(this.CheckUpdateConditions());
                return;
            }
            Hashtable data = new Hashtable();
            foreach (var field in typeof(User).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (field.Name == "updated_at")
                {
                    data.Add(field.Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                }
                else if (field.Name != "database" && !string.IsNullOrEmpty(field.GetValue(this).ToString()))
                {
                    data.Add(field.Name, field.GetValue(this));
                }
            }
            database.UpdateData(data, "Users");
        }

        public Hashtable GetUserInfo()
        {
            Hashtable Info = new Hashtable();
            foreach (var field in typeof(User).GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                Info.Add(field.Name, field.GetValue(this));
            }
            return Info;
        }


        public bool Authenticate()
        {
            string query = "SELECT Users.ID,Users.typeofuser FROM Users ";
            query += "JOIN Residents ON (Residents.ID = Users.Resident_id)";
            query += "WHERE username = '" + this.username + "' AND password = '" + this.password + "'";
            query += " AND IFNULL(Residents.EndDate,'" + DateTime.Today.ToString("yyyy-MM-dd") + "') >= ";
            query += "'" + DateTime.Today.ToString("yyyy-MM-dd") + "'";

            DataTable data = database.GetData(query);
            if (data.Rows.Count == 0 || data.Rows[0][0] == DBNull.Value)
                return false;
            else
            {
                this.ID = Convert.ToInt32(data.Rows[0][0]);
                this.typeofuser = Convert.ToByte(data.Rows[0][1]);
                return true;
            }
        }

        public Hashtable GetResidentInfo()
        {
            StringBuilder query = new StringBuilder();
            Hashtable Info = new Hashtable();
            query.AppendLine("SELECT * FROM Residents WHERE ID IN (");
            query.AppendLine("SELECT Resident_id FROM Users WHERE ID = " + this.ID.ToString() + ")");
            DataTable data = database.GetData(query.ToString());
            if (data.Rows.Count > 0 && data.Rows[0][0] != DBNull.Value)
            {
                foreach (DataColumn column in data.Columns)
                {
                    Info.Add(column.ColumnName, data.Rows[0][column]);
                }
            }
            return Info;
        }
        public static Hashtable GetResidentInfoByUserId(int id)
        {
            StringBuilder query = new StringBuilder();
            Hashtable Info = new Hashtable();
            query.AppendLine("SELECT * FROM Residents WHERE ID IN (");
            query.AppendLine("SELECT Resident_id FROM Users WHERE ID = " + id.ToString() + ")");
            DataTable data = database.GetData(query.ToString());
            if (data.Rows.Count > 0 && data.Rows[0][0] != DBNull.Value)
            {
                foreach (DataColumn column in data.Columns)
                {
                    Info.Add(column.ColumnName, data.Rows[0][column]);
                }
            }
            return Info;
        }
        private string CheckInsertConditions()
        {
            return string.Empty;
        }
        private string CheckUpdateConditions()
        {
            return string.Empty;
        }
        private string CheckDeleteConditions()
        {
            return string.Empty;
        }

        private void AssignUsernamePassword(string firstname)
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] passString = new char[8];
            char[] userString = new char[3];
            Random random = new Random();

            for (int i = 0; i < passString.Length; i++)
            {
                passString[i] = chars[random.Next(chars.Length)];
            }

            for (int i = 0; i < userString.Length; i++)
            {
                userString[i] = chars[random.Next(chars.Length)];
            }

            this.password = new String(passString);
            this.username = firstname + new String(userString);
        }

        public static DataTable GetAllUsers()
        {
            return database.GetData("SELECT * FROM Users WHERE ID NOT IN (-1,-2)");
        }
    }
}
